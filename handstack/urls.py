from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

import settings

import os


from django.contrib.auth.models import User
from event.models import Event
from rest_framework import routers, serializers, viewsets,filters

import django_filters

from rest_framework import generics



class EventFilter(django_filters.FilterSet):
    min_lat = django_filters.NumberFilter(name="latitude", lookup_type='gte')
    max_lat = django_filters.NumberFilter(name="latitude", lookup_type='lte')
    min_lon = django_filters.NumberFilter(name="longitude", lookup_type='gte')
    max_lon = django_filters.NumberFilter(name="longitude", lookup_type='lte')
    class Meta:
        model = Event
        fields = ('eventName', 'eventAddress','latitude','longitude','eventComputedStartTime', 'EventRSVPS')




# Serializers define the API representation.


# Serializers define the API representation.
class UserSerializer(serializers.PrimaryKeyRelatedField):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')



class EventSerializer(serializers.HyperlinkedModelSerializer):
    EventRSVPS=UserSerializer(many=True, read_only=True)
    class Meta:
        model = Event
        fields = ('eventName', 'eventAddress','latitude','longitude','eventComputedStartTime', 'EventRSVPS')
        

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

class eventsearch(generics.ListAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_class = EventFilter


router = routers.DefaultRouter()
router.register(r'events', EventViewSet)
#router.register(r'events', eventsearch)
router.register(r'users', UserViewSet)




BASE_DIR = os.path.dirname(os.path.dirname(__file__))



urlpatterns = patterns('',
    # ios app

    
    url(r'^search/$', 'event.views.search', name='search'),
    # GET, lots of optional arguments

    url(r'^taskdump/$', 'event.views.taskdump', name='taskdump'),

    url(r'^fixtz/$', 'event.views.fixalltz', name='fixtz'),

    url(r'^getemails/$', 'event.views.getemails', name='eget'),
    url(r'^blockemails/$', 'event.views.blockemails', name='eblock'),
    url(r'^unsubscribe/$', 'event.views.unsubscribe', name='unsubscribe'),
    
#REST API

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(router.urls)),
#    url(r'^apisearch/$', 'eventsearch', name='eventsearch'),

#END REST API



    url(r'^tasksearch/$', 'event.views.tasksearch', name='tasksearch'),
    # GET tasksearch/?eventid=4 returns task in event 4
    url(r'^make/$', 'event.views.mkevent', name='make'),
    # POST makes events 
    url(r'^eventupdate/$', 'event.views.eventupdate', name='eventupdate'),

    url(r'^maketask/$', 'event.views.mktask', name='maketask'),

    url(r'^mki/$', 'event.views.mki', name='mki'),

    url(r'^frontline/$', 'event.views.frontline', name='fakelogin'),

    url(r'^register/$', 'event.views.userregister', name='registration'),

    url(r'^login/$', 'event.views.userlogin', name='login'),

    url(r'^logout/$', 'event.views.logout_view', name='logout'),

    url(r'^whoami/$', 'event.views.whoami', name='whoami'),
       #returns uid if logged in 
    url(r'^rsvp/$', 'event.views.rsvpme', name='rsvp'),
    # 
    url(r'^unrsvp/$', 'event.views.unrsvpme', name='unsrvp'),

    url(r'^setstatus/$', 'event.views.updatetaskstatus', name='taskstatus'),
    url(r'^myevents/$', 'event.views.myevents', name='myevents'),

    url(r'^taketask/$', 'event.views.taketask', name='taketask'),

    url(r'^droptask/$', 'event.views.droptask', name='droptask'),

    url(r'^mytasks/$', 'event.views.mytasks', name='mytasks'),

    url(r'^givetask/$', 'event.views.givetask', name='givetask'),

    url(r'^taskupdate/$', 'event.views.taskupdate', name='taskupdate'),

    url(r'^takeinterest/$', 'event.views.takeinterest', name='takeinterest'),

    url(r'^dropinterest/$', 'event.views.dropinterest', name='dropinterest'),

    url(r'^rmevent/$', 'event.views.rmevent', name='rmevent'),

    url(r'^mkgroup/$', 'event.views.mkgroup', name='mkgroup'),

    url(r'^listgroups/$', 'event.views.listgroups', name='listgroups'),

    url(r'^joingroup/$', 'event.views.mkgroup', name='joingroup'),

    url(r'^leavegroup/$', 'event.views.mkgroup', name='leavegroup'),

    url(r'^kickgroup/$', 'event.views.mkgroup', name='kickgroup'),

    url(r'^addorgadmin/$', 'event.views.addorgadmin', name='addadmin'),

    url(r'^rmorgadmin/$', 'event.views.rmorgadmin', name='rmadmin'),
    
    url(r'^mkcomment/$', 'event.views.mkcomment', name='mkcomment'),

    url(r'^comments/$', 'event.views.comments', name='mkcomment'),

    url(r'^rmcomment/$', 'event.views.rmcomment', name='mkcomment'),

    url(r'^lookupuid/$', 'event.views.LookUpUid', name='luuid'),

    url(r'^lookupuser/$', 'event.views.LookUpUserName', name='luun'),

    url(r'^listinterests/$', 'event.views.listinterests', name='listinterests'),

    url(r'^eventimage/$', 'event.views.eventimage', name='eventimage'),

    url(r'^image/$', 'event.views.eventimage', name='eventimage'),

    url(r'^geteventimage/$', 'event.views.geteventimage', name='geteventimage'),

    url(r'^getimage/$', 'event.views.geteventimage', name='geteventimage'),

##One time hacks
   
    url(r'^makenotices/$', 'event.views.create_notifications_once', name='makenotices'),


    # web app url routes

    url(r'^login_user/$', 'event.views.login_user', name='login_user'),
    url(r'^logout_user/$', 'event.views.logout_user', name='logout_user'),
    url(r'^register_user/$', 'event.views.register_new_user', name='register_user'),

    url(r'^create_event/$', 'event.views.create_event', name='create_event'),
    url(r'^edit_event/$', 'event.views.edit_event', name='edit_event'),
    url(r'^remove_event/$', 'event.views.remove_event', name='remove_event'),
    url(r'^get_events/$', 'event.views.get_events', name='get_events'),
    

    url(r'^create_task/$', 'event.views.create_task', name='create_task'),
    url(r'^edit_task/$', 'event.views.edit_task', name='edit_task'),
    url(r'^remove_task/$', 'event.views.remove_task', name='remove_task'),
    url(r'^get_tasks_for_event/$', 'event.views.get_tasks_for_event', name='get_tasks_for_event'),
    url(r'^get_task_for_taskid/$', 'event.views.get_task_for_taskid', name='get_task_for_taskid'),
    url(r'^assign_task_to_user/$', 'event.views.assign_task_to_user', name='assign_task_to_user'),
    url(r'^get_assignments_for_task/$', 'event.views.get_assignments_for_task', name='get_assignments_for_task'),

    url(r'^get_active_eventids/$', 'event.views.get_active_eventids', name='get_active_eventids'),

    url(r'^get_rsvps_for_event/$', 'event.views.get_rsvps_for_event', name='get_rsvps_for_event'),
    url(r'^rsvp_user_for_event/$', 'event.views.rsvp_user_for_event', name='rsvp_user_for_event'),

    url(r'^create_eventcomment/$', 'event.views.create_eventcomment', name='create_eventcomment'),
    url(r'^edit_eventcomment/$', 'event.views.edit_eventcomment', name='edit_eventcomment'),
    url(r'^remove_eventcomment/$', 'event.views.remove_eventcomment', name='remove_eventcomment'),
    url(r'^get_eventcomments_for_event/$', 'event.views.get_eventcomments_for_event', name='get_eventcomments_for_event'),

    url(r'^create_taskcomment/$', 'event.views.create_taskcomment', name='create_taskcomment'),
    url(r'^edit_taskcomment/$', 'event.views.edit_taskcomment', name='edit_taskcomment'),
    url(r'^remove_taskcomment/$', 'event.views.remove_taskcomment', name='remove_taskcomment'),
    url(r'^get_taskcomments_for_task/$', 'event.views.get_taskcomments_for_task', name='get_taskcomments_for_task'),

    # admin, statics, etc.
    url(r'^bloorp/', include(admin.site.urls)),

    url(r'^blaap/(?P<path>.*)$', 'django.views.static.serve',        {'document_root': '/var/django/stylefiles'}),

    url(r'^$', 'event.views.appserve', name='appserve'),
    url(r'^app/$', 'event.views.serve_app', name='serve_app'),

    url(r'^(?P<path>.*\.html)$', 'event.views.htmlserve', name='htmlserve' ),
    url(r'^(?P<path>.*\.css)$', 'event.views.cssserve', name='cssserve' ),

    url(r'^(\d+)$', 'event.views.htmlevent',  name='htmlevent'),
    
#     if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    url(r'^uploads/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),
    
    url(r'^(?P<path>.*)$', 'django.views.static.serve',        {'document_root': BASE_DIR+'/static'}),


)

