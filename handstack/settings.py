"""
Django settings for handstack project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))



###Email settings
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25


###Notification settings

SITE_ID= 1
DEFAULT_FROM_EMAIL="hello@handstack.org"


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0ghqj_2dThis is the newsecret. well mostlynew.. 72878078 6737840 nq'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['*']

# Image Settings in settings.py                                                                                                                                                                                                                                                                                                                                
FILE_UPLOAD_MAX_MEMORY_SIZE = 2.5

# Directory where files larger than the above size are stored                                                                                                                                                                                                                                                                                                   


# Operating permissions for uploaded files (default 0600)                                                                                                                                        
FILE_UPLOAD_PERMISSIONS = 0644


TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
)


TEMPLATE_DIRS= ( BASE_DIR+'/static', )# *okoa*
#TEMPLATE_DIRS = ( BASE_DIR+'/event/templates/', )


TEMPLATE_CONTEXT_PROCESSORS =(
        'django.contrib.messages.context_processors.messages',
        'django.contrib.auth.context_processors.auth',
        "django.core.context_processors.request",
        "django.core.context_processors.debug",
        "django.core.context_processors.i18n",
        "django.core.context_processors.media",
        "django.core.context_processors.static",
        "django.core.context_processors.tz",
)

# Application definition

INSTALLED_APPS = (
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.sites',
        'django_pdb',
        'social_auth',
        'event',
        'imagekit',
        'south',
        'crispy_forms',
        'notification',
        'reversion',
        'rest_framework'

)
AUTHENTICATION_BACKENDS = ['social_auth.backends.OpenIDBackend',
                           'django.contrib.auth.backends.ModelBackend']
REST_FRAMEWORK = {
        'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',)
}


CRISPY_TEMPLATE_PACK = 'bootstrap3'
CRISPY_FAIL_SILENTLY = not DEBUG
MIDDLEWARE_CLASSES = (
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django_pdb.middleware.PdbMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'handstack.urls'

WSGI_APPLICATION = 'handstack.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

import handstack.hsdbinfo

DATABASES = handstack.hsdbinfo.DATABASES
MEDIA_ROOT = handstack.hsdbinfo.MEDIA_ROOT
MEDIA_URL = handstack.hsdbinfo.MEDIA_URL
STATIC_URL = handstack.hsdbinfo.STATIC_URL
STATIC_ROOT = handstack.hsdbinfo.STATIC_ROOT
FILE_UPLOAD_TEMP_DIR = handstack.hsdbinfo.FILE_UPLOAD_TEMP_DIR

if hasattr(handstack.hsdbinfo, 'SECRET_KEY'):
        SECRET_KEY = handstack.hsdbinfo.SECRET_KEY
        if hasattr(handstack.hsdbinfo, 'DEBUG'):
                DEBUG = handstack.hsdbinfo.DEBUG
                if hasattr(handstack.hsdbinfo, 'TEMPLATE_DEBUG'):
                        TEMPLATE_DEBUG = handstack.hsdbinfo.TEMPLATE_DEBUG
                        if hasattr(handstack.hsdbinfo, 'ALLOWED_HOSTS'):
                                ALLOWED_HOSTS = handstack.hsdbinfo.ALLOWED_HOSTS

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True
