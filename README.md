HandStack
========

Web frontend and backend for HandStack.

(C) 2014-present. All rights reserved.

To setup:

1. *virtualenv env*

2. *./env/bin/pip install -r ./requirements.txt*

3. *./env/bin/python ./manage.py syncdb*

4. *./env/bin/python ./manage.py migrate*

5. *./env/bin/python ./manage.py runserver 0.0.0.0:8000*

Now point a browser on any machine to the http://the.server.ip:8000/
and you should see the homepage.