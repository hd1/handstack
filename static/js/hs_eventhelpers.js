// call initialize_forms method once the page is ready!
$(document).ready( (function() { 
	console.log("READY!");
	init_all_events();
    $('.button').click(function(e) {
        e.stopPropagation();
    });


    $('#warnModal').on('shown.bs.modal', function (e) {
  		// do something...
		console.log("SHOWN!");
	});
}));


function taskready(){

$('.statcheck').unbind().change(function(){
console.log( $(this).attr("name"));
console.log( $(this).is(':checked'));
tid=$(this).attr("name");
status=$(this).is(':checked');
tstat="False";
if ($(this).is(':checked')){tstat="True";}

$.ajax({
        global: false,
        type: "POST",
        url: "/setstatus/",
        data: {'taskid': tid, 'taskStatus': tstat },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (!(json['success'])) {
                // render error
                $("#login.error").html(json['error']);
            } else {
                console.log("status set ");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
});

}





function ajaxurl(hash){
    if (hash){
        console.log (hash);
	$(".event").hide();
	$(hash+".event").show();
	
		//Re-center-map
        var lat = $(hash+".event").find(".latitude").text();
        var lon = $(hash+".event").find(".longitude").text();
        center_map_to_coordinates(lat, lon);
map.setZoom(16);
eid=hash.replace(/#/, '');
$("#expand-event-"+eid).removeClass("collapse");




	}
}	

function show_all_events(){
 $(".event").show();
}



function show_login_modal() {
	console.log("SHOW LOGIN");
	$("#warnModal").find(".modal-header").html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button><h4>Please log in or <a class=\"btn btn-link \" id=\"switch-to-register\" onclick=\"show_register_modal()\">register</a>.</h4>");
	$("#warnModal").find(".modal-body").html("<div class=\"md-col-4\">Username:</div><div class=\"md-col-6\"><input id=\"username\" name=\"username\"></div></div><div class=\"md-col-4\">Password:</div><div class=\"md-col-6\"><input id=\"password\" name=\"password\" type=\"password\"></div><div id='loginerror' class=\"md-col-6 error\"></div></div>");
	$("#warnModal").find(".modal-footer").html("<p><button class=\"btn btn-volunteer\" onclick=\"attempt_login()\">Log In</button></p>");
	var opts = {"backdrop": "false"};
	$("#warnModal").modal(opts);
}

function show_register_modal () {
	console.log("SHOW LOGIN");
	$("#warnModal").find(".modal-header").html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button><h4>Please register or <a class=\"btn btn-link \" id=\"switch-to-login\" onclick=\"show_login_modal()\">log in</a>.</h4>");
	$("#warnModal").find(".modal-body").html("<div class=\"md-col-4\">Username:</div><div class=\"md-col-6\"><input class=\"textinput\" id=\"username\" name=\"username\"></div><div class=\"md-col-4\">Email:</div><div class=\"md-col-6\"><input class=\"textinput\" id=\"email\" name=\"email\"></div></div><div class=\"md-col-4\">Password:</div><div class=\"md-col-6\"><input id=\"password\" name=\"password\" type=\"password\"></div><div id='regerror' class=\"md-col-6 error\"></div></div>");
	$("#warnModal").find(".modal-footer").html("<p><button class=\"btn btn-volunteer\" onclick=\"attempt_register()\">Register</button></p>");
    var opts = {"backdrop": "false"};
	$("#warnModal").modal(opts);
	// 	<div class=\"md-col-4 error\" id=\"register\"></div>
}

function attempt_login() {
	console.log("try login");
	$.ajax({
        global: false,
        type: "GET",
        url: "/login_user/",
        data: {'username': $("#username").val(), 'password': $("#password").val() },
        dataType: "json",
        success: function (json) {
        	console.log("attempt login...");
        	console.log(json);
        	if (!(json['success'])) {            	
            	// render error
				$("#loginerror.error").html(json['error']);
            } else {
                console.log("Success! Now reload for user with id: ", json['userid']);
                location.reload(true);
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
	return false;
}

function attempt_register() {
	console.log("try register");
	$.ajax({
        global: false,
        type: "GET",
        url: "/register_user/",
        data: {'username': $("#username").val(), 'password': $("#password").val(), 'email': $("#email").val() },
        dataType: "json",
        success: function (json) {
        	console.log("attempt register...");
        	console.log(json);
        	if (!(json['success'])) {            	
            	// render error
				$("#regerror.error").html(json['error']);
            } else {
                console.log("Success! Now reload for user with id: ", json['userid']);
                location.reload(true);
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
	return false;
}


function logout() {
	console.log("LOGOUT!");
	$.ajax({
        global: false,
        type: "GET",
        url: "/logout_user/",
        dataType: "json",
        success: function (json) {
        	console.log("attempted logout...");
        	console.log(json);
        	if (!(json['success'])) {
				console.log("There was an error with logout!!! Error: ", json['error']);
            } else {
                console.log("Success! Now reload for user with NO id...");
                location.reload(true);
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
	return false;
}




// Events!
function create_event(){
    console.log("create event form...");
	$.ajax({
        global: false,
        type: "GET",
        url: "/create_event/",
        dataType: "json",
        success: function (json) {
        	console.log("create event...");
        	console.log(json);
        	if (!(json['success'])) {
            	$("#999999.event").html(json['form_html']);
            	console.log(json['form_html']);
            	register_event_create_callbacks(999999);
            } else {
                console.log("create_event success error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function register_event_create_callbacks(eid){
    $('.datetimeparentdiv input').datetimepicker({'format': 'Y/m/d H:i'});
    $( "form" ).on( "submit", false );
    $('form :button.btn-warning').on('click', function(e) {
        e.preventDefault();
        $(this).parents('.event').html('');
    });
    $('form :submit.ajax_submit').on('click', function(e) {
        e.preventDefault();
        var fd = new FormData();
        var filedata = $("#"+eid.toString()+".event").find('input[type="file"]')[0].files[0];
        var formdata = $(this).parents('#id-eventForm').serializeArray();
        $.each(formdata,function(key,input){
            fd.append(input.name,input.value);
        });
        fd.append("image", filedata);
        submit_event(eid, fd, "/create_event/"); // HANDLE GEOCODING ON BACK END!
        return false;
    });
}

function edit_event(eid){
    console.log("update event form: ", eid.toString());
	$.ajax({ 
        global: false,
        type: "GET",
        url: "/edit_event/",
        data: {"eventid": eid.toString()},
        dataType: "json",
        success: function (json) {
        	console.log("edit event comment...");
        	console.log(json);
        	if (!(json['success'])) {
                $("#"+eid.toString()+".event").html(json['form_html']);
            	console.log(json['form_html']);
        	    register_event_edit_callbacks(eid);
            } else {
                console.log("edit-event success error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function register_event_edit_callbacks(eid){
    $('.datetimeparentdiv input').datetimepicker({'format': 'Y/m/d H:i'});
    $( "form" ).on( "submit", false );
    $('form :button.btn-warning').on('click', function(e) {
        e.preventDefault();
        get_event_for_eventid(eid); // todo: just grab one!
    });
    $('form :submit.ajax_submit').on('click', function(e) {
        e.preventDefault();
        var fd = new FormData();
        var filedata = $("#"+eid.toString()+".event").find('input[type="file"]')[0].files[0];
        fd.append("image", filedata);
        var formdata = $(this).parents('#id-eventForm').serializeArray();
        $.each(formdata,function(key,input){
            fd.append(input.name,input.value);
        });
        fd.append("eventid", eid.toString());
        submit_event(eid, fd, "/edit_event/"); // HANDLE GEOCODING ON BACK END!    
        return false;
    });
}

function submit_event(eid, fd, url) {
    $.ajax({
        global: false,
        type: "POST",
        url: url,
        data: fd,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (json) {
            console.log("submit event...");
            console.log(json);
            if (!(json['success'])) {
                $("#"+eid.toString()+".event").html(json['form_html']);
                register_event_create_callbacks(eid);
            } else {
                console.log("create/edit event -- success! ... "+eid.toString());
                $('#'+eid.toString()+'.event').html('');
                if (url == "/create_event/") {
                    init_all_events();
                } else {
                    get_event_for_eventid(eid);
                }
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
    console.log("RETURN FROM SUBMIT");
}


function get_all_eventids(init_divs){
	$.ajax({
        global: false,
        type: "GET",
        url: "/get_active_eventids/",
        data: {'divs_flag' : init_divs},
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("got all the eventids for events as json...");
                if (init_divs) {
                    console.log(json['events_html']);
                    $(".eventlist").html(json['event_html']);
                }            
                for (evid in json['active_event_list']) {
                    console.log(json['active_event_list'][evid])
                    get_event_for_eventid(json['active_event_list'][evid]);
                }
            } else {
                console.log("get-all-eventids error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
    return false;
}

function setup_click_events_for_all_events() {
    $(".btn-eventdetails").click(function(){
        var targetid = $(this).attr("id");
        console.log( targetid );
        var lat = $("#"+targetid.toString()+".event").find(".latitude").text();
        var lon = $("#"+targetid.toString()+".event").find(".longitude").text();
        center_map_to_coordinates(lat, lon);
    });

}

function init_all_events(){
    get_all_eventids(true);
    return false;
}

function finish_events_display(){
    place_all_event_markers();      // local
    sort_all_events_by_epoch();
    setup_click_events_for_all_events();
    ajaxurl(window.location.hash);
}

$( document ).ajaxStop(function() {
    console.log( "\n\n\nTriggered ajaxStop handler.\n\n\n");
    finish_events_display();
});

function get_event_for_eventid(eid){
	$.ajax({
        type: "GET",
        url: "/get_events/",
        data: {"eventid": eid.toString()},
        dataType: "json",
        success: function (json) {
        	console.log("get ONE!");
            if (json['success']) {
            	console.log("JSON: ");
            	console.log(json);
            	// read into eventlist div
            	$("#"+eid.toString()+".event").replaceWith(json['event_html']);
            	get_tasks_for_event(eid);
            	get_rsvps_for_event(eid);
                get_eventcomments_for_event(eid);

            } else {
                console.log("get event error: \n"+json);
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
    return false;
}

function sort_all_events_by_epoch(){
    var divlist = $(".event").not("#999999");
    divlist.sort(function(a,b) { return $(b).data("startepoch") - $(a).data("startepoch") });
    $(".eventlist").html(divlist);
}

function sort_all_tasks_by_priority(eid){
    var divlist = $("#"+eid.toString()+".tasklist").children();
    divlist.sort(function(a,b) { return $(b).data("priority") - $(a).data("priority") });
    $("#"+eid.toString()+".tasklist").html(divlist);
}


function center_map_to_coordinates(lat, lon) {
    var center = new google.maps.LatLng(lat, lon);
    map.panTo(center);
}

// $( "#target" ).click(function() { alert( "Handler for .click() called." ); });

function place_all_event_markers(){

    var myLatlng14222 = new google.maps.LatLng(42.914903,-78.875064);
    var mapOptions = {
        zoom: 12,
        center: myLatlng14222,
        streetViewControl: false
    }
    map = new google.maps.Map(document.getElementById("map_cont"), mapOptions);
    
    $(".event").each( function(i, v){
        console.log(i.toString()+"|"+v.id.toString());
        var id = v.id;
        var title = $("#"+v.id.toString()+".event").find(".eventName").text();
        var lat = $("#"+v.id.toString()+".event").find(".latitude").text();
        var lon = $("#"+v.id.toString()+".event").find(".longitude").text();
        console.log([title, lat, lon]);
        var evlatlng = new google.maps.LatLng(lat, lon);
        
        var image = {
            url : "img/marker16.png"
//             size : new google.maps.Size(20,20),
//             origin: new google.maps.Point(0,0),
//             anchor: new google.maps.Point(0,20)
        };
        var marker=new google.maps.Marker({
            position    : evlatlng,
            map         : map,
            title       : title,
            icon        : image
        });
        
        var infowindow = new google.maps.InfoWindow({
            content: title
        });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(marker.get('map'), marker);
        });
        
        google.maps.event.addListener(marker, 'dblclick', function() {
            map.setCenter(marker.getPosition());
        });        
    });
}

function remove_event(eid){
    console.log("EID: ", eid);
	$.ajax({
        global: false,
        type: "GET",
        url: "/remove_event/",
        data: { "eventid" : eid.toString() },
        dataType: "json",
        success: function (json) {
        	console.log("remove!");
        	console.log(json);
            if (json['success']) {
            	// WARNING $("#responsorial").html(json);
            	init_all_events();
            } else {
                console.log("remove-event error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

// function geocode_address(){
// 	   	
// 	adr=$("#id_eventAddress.textinput").val();
// 	console.log(adr);
// 	if (adr == "") {
//    		adr = "400 Main Street, Buffalo, NY 14202, USA";
// 	} else {
// 		console.log("Use input Geolocation!");
// 		console.log(adr);
// 	}
// 	
// 	var geocoder = new google.maps.Geocoder();	
//   	geocoder.geocode( { 'address': adr}, function(r, status) {
// 
//     	if (status == google.maps.GeocoderStatus.OK) {
//             console.log('--------------------------');
//       		console.log(r);
//     	  	console.log( $("#id_eventAddress.textinput").val() );
//     	  	console.log( $("#id_latitude.textinput").val() );
//     	  	console.log( $("#id_longitude.textinput").val() );
//     	  	$("#id_eventAddress.textinput").val(r[0].formatted_address);
//     	  	$("#id_latitude.textinput").val((r[0].geometry.location.k).toFixed(6));
//     	  	$("#id_longitude.textinput").val((r[0].geometry.location.B).toFixed(6));
//             console.log("result:");
//     	  	console.log( $("#id_eventAddress.textinput").val() );
//     	  	console.log( $("#id_latitude.textinput").val() );
//     	  	console.log( $("#id_longitude.textinput").val() );
//     	  	
//     	} else {
//     	    console.log("geo error");
//             console.log(status);
//     	}
// 	});
// }


//RSVPS
function get_all_rsvps(){
    console.log("Get all tasks for all events.");
    $.ajax({
        global: false,
        type: "GET",
        url: "/get_active_eventids/", // this can be overloaded with criteria args!
//             data: {},
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("got all the eventids for tasks as json...");
                // iterate over that data and read into tasklist (sub)containers
                // change to json["eventids"] to be sure?
                for (evid in json['active_event_list']) {
                    console.log(json['active_event_list'][evid])
                    get_rsvps_for_event(json['active_event_list'][evid]);
                }
            } else {
                console.log("get-all-rsvps error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function get_rsvps_for_event(eid){
    $.ajax({
        global: false,
        type: "GET",
        url: "/get_rsvps_for_event/",
        data: { "eventid" : eid.toString() },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("should get "+eid.toString()+"'s rsvps");
                // read into #rsvplist
                $("#"+eid.toString()+".rsvplist").html(json['rsvp_html']);
            } else {
                console.log("get-rsvps-for-event error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function rsvp_to_event(eid){
    console.log("rsvp: "+eid.toString());
	$.ajax({
        global: false,
        type: "GET",
        url: "/rsvp_user_for_event/",
        data: { "eventid": eid.toString(), "addflag": "1" },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
            	get_rsvps_for_event(eid);
            } else {
                console.log("RSVP-to-event error.");
            }
        },
        error: function (resp) {
            console.log("ERROR: "+resp.responseText);
        }
    });
    return false;
}

function unrsvp_to_event(eid){
    console.log("unrsvp: " + eid.toString()); //
	$.ajax({
        global: false,
        type: "GET",
        url: "/rsvp_user_for_event/",
        data: { "eventid": eid.toString(), "addflag": "0" },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
            	get_rsvps_for_event(eid);
            } else {
                console.log("UN-rsvp-to-event error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
    return false;
}


// tasks
function create_task(eid, placeholder_tid){
    console.log("create task form: ", eid.toString());
    var placeholder_tid = eid * -1;
	$.ajax({
	    global: false,
        type: "GET",
        url: "/create_task/",
        dataType: "json",
        success: function (json) {
        	console.log("create task...");
        	console.log(json);
        	if (!(json['success'])) {
            	$("#"+placeholder_tid.toString()+".task").html(json['form_html']);
            	console.log(json['form_html']);
            	register_task_create_callbacks(eid, placeholder_tid);
            } else {
                console.log("create_task success error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function register_task_create_callbacks(eid, placeholder_tid){
    $('.datetimeparentdiv input').datetimepicker({'format': 'Y/m/d H:i'});
    $( "form" ).on( "submit", false );
    $('form :button.btn-warning').on('click', function(e) {
        e.preventDefault();
        console.log( $(this).parents('.task').html('')  );
    });
    $('form :submit.ajax_submit').on('click', function(e) {
        e.preventDefault();
        var formdata = $(this).parents('#id-taskForm').serializeArray();
        console.log(eid.toString());
        formdata.push({"name" : "eventid", "value": eid.toString()});
        console.log(formdata);
        $.ajax({
    	    global: false,
            type: "POST",
            url: "/create_task/",
            data: formdata,
            dataType: "json",
            success: function (json) {
                console.log("submit task...");
                console.log(json);
                if (!(json['success'])) {
                    $("#"+placeholder_tid.toString()+".task").html(json['form_html']);
                    register_task_create_callbacks(eid, placeholder_tid);
                } else {
                    console.log("create task -- success!");
                    $('#'+placeholder_tid.toString()+'.task').html(''); // json['parentEventID']
                    get_tasks_for_event(eid); // todo: just grab one! json['parentEventID']
                }
            },
            error: function (resp) {
                console.log(resp.responseText);
            }
        });
        return false;
    });
}

function edit_task(tid){
    console.log("update task form: ", tid.toString());
	$.ajax({
	    global: false,
        type: "GET",
        url: "/edit_task/",
        data: {"taskid": tid.toString()},
        dataType: "json",
        success: function (json) {
        	console.log("edit event comment...");
        	console.log(json);
        	if (!(json['success'])) {
                $("#"+tid.toString()+".task").html(json['form_html']);
        	    register_task_edit_callbacks(tid);
            } else {
                console.log("edit-task success error/");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function register_task_edit_callbacks(tid){
    $('.datetimeparentdiv input').datetimepicker({'format': 'Y/m/d H:i'});
    $( "form" ).on( "submit", false );
    $('form :button.btn-warning').on('click', function(e) {
        e.preventDefault();
        get_task_for_taskid(tid);
    });
    $('form :submit.ajax_submit').on('click', function(e) {
        e.preventDefault();
        var formdata = $(this).parents('#id-taskForm').serializeArray();
        console.log(tid.toString());
        formdata.push({"name" : "taskid", "value": tid.toString()});
        console.log(formdata);
        $.ajax({
	        global: false,
            type: "POST",
            url: "/edit_task/",
            data: formdata,
            dataType: "json",
            success: function (json) {
                console.log("edit task...");
                console.log(json);
                if (!(json['success'])) {
                    $("#"+tid.toString()+".task").html(json['form_html']);
                    console.log(json['form_html']);
            	    register_task_edit_callbacks(tid);
                } else {
                    console.log("edit task -- success!");
                    $('#'+json['parentEventID'].toString()+'.task').html('');
                    get_tasks_for_event(json['parentEventID']);
                }
            },
            error: function (resp) {
                console.log(resp.responseText);
            }
        });
        return false;
    });
}


function get_all_tasks(){
    console.log("Get all tasks for all events.");
    $.ajax({
    	global: false,
        type: "GET",
        url: "/get_active_eventids/", // this can be overloaded with criteria args!
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("got all the eventids for tasks as json...");
                // iterate over that data and read into tasklist (sub)containers
                // change to json["eventids"] to be sure?
                for (evid in json['active_event_list']) { 
                    get_tasks_for_event(json['active_event_list'][evid]);
                }
            } else {
                console.log("get-all-tasks error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}
    
function get_tasks_for_event(eid){
   $.ajax({
    	global: false,
        type: "GET",
        url: "/get_tasks_for_event/",
        data: { "eventid" : eid.toString() },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("should get "+eid.toString()+"'s tasks");
                // read into #tasklist
                $("#"+eid.toString()+".tasklist").html(json['task_html']);
                for (tid in json['taskids']) {
                    get_taskcomments_for_task(json['taskids'][tid]);
                    sort_all_tasks_by_priority(eid);
                    taskready();
                }
            } else {
                console.log("get-tasks-for-event error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function get_task_for_taskid(tid){
    $.ajax({
    	global: false,
        type: "GET",
        url: "/get_task_for_taskid/",
        data: { "taskid" : tid.toString() },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("should get "+tid.toString()+" (a single task)");
                // read into #tasklist
                $("#"+tid.toString()+".task").html(json['task_html']);
                console.log("task ids: "+json['taskids']);
                $("[data-toggle=tooltip]").tooltip();
                for (tid in json['taskids']) {
                    get_taskcomments_for_task(json['taskids'][tid]);
                    // bubble up and sort by priority
                    sort_all_tasks_by_priority( $(this).parents('.event').id );
                }
            } else {
                console.log("get-task-for-taskid error: \n"+json);
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function remove_task_by_taskid(tid){
    // assume that tids are unique across all tasks!
    // walk up the tree to the first event div and get it's id!
    console.log("tid: "+tid);
    var eid = $("#"+tid+".task").closest("div.event").attr("id");
    console.log("removing: "+eid);
	$.ajax({
    	global: false,
        type: "GET",
        url: "/remove_task/",
        data: { "eventid" : eid, "taskid" : tid },
        dataType: "json",
        success: function (json) {
         	console.log(json);
        	console.log("remove!");
            if (json['success']) {
            	get_tasks_for_event(eid);
            } else {
                console.log("remove-task error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}


//
// eventcomments
//
function create_eventcomment(placeholder_eid){ // -id
    console.log("create event comment form: ", placeholder_eid.toString()); // -id
	$.ajax({
    	global: false,
        type: "GET",
        url: "/create_eventcomment/",
        data: {'eventid': (-1 * placeholder_eid) }, // back to +id
        dataType: "json",
        success: function (json) {
        	console.log("create event comment...");
        	console.log(json);
        	if (!(json['success'])) {
            	$("#"+placeholder_eid.toString()+".eventcomment").html(json['form_html']); // -id
            	register_ec_create_callbacks(placeholder_eid); // -id
            } else {
                console.log("create_eventcomment success error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function register_ec_create_callbacks(placeholder_eid){ // -id
//     $('.datetimeparentdiv input').datetimepicker({'format': 'Y/m/d H:i'});
    $( "form" ).on( "submit", false );
    $('form :button.btn-warning').on('click', function(e) {
        e.preventDefault();
        // placeholder_eid is a new comment's ecid until it's in the db and a fresh get
        console.log('#'+placeholder_eid.toString()+'.eventcomment'); // -id
        console.log( $('#'+placeholder_eid.toString()+'.eventcomment') ); // -id
        $(this).parents('#'+placeholder_eid.toString()+'.eventcomment').html(''); // -id

    });
    $('form :submit.ajax_submit').on('click', function(e) {
        e.preventDefault();
        console.log('#'+placeholder_eid.toString()+'_id-commentForm'); // -id
        var formdata = $('#'+placeholder_eid.toString()+'_id-commentForm').serializeArray(); // -id
        
        formdata.push({"name" : "eventid", "value": (-1*placeholder_eid).toString()}); // +id
        console.log(formdata);
        $.ajax({
    	    global: false,
            type: "POST",
            url: "/create_eventcomment/",
            data: formdata,
            dataType: "json",
            success: function (json) {
                console.log("submit event comment...");
                console.log(json);
                if (!(json['success'])) {
                    $("#"+placeholder_eid.toString()+".eventcomment").html(json['form_html']); // -id
                    register_ec_create_callbacks(placeholder_eid); // -id
                } else {
                    console.log("create event comment -- success!");
                    $('#'+placeholder_eid.toString()+'.eventcomment').html('');  // -id
                    get_eventcomments_for_event(-1 * placeholder_eid); // +id
                }
            },
            error: function (resp) {
                console.log(resp.responseText);
            }
        });
        return false;
    });
}

function edit_eventcomment(ecid){ // +1
    console.log("update event comment form: ", ecid.toString()); //+1
	$.ajax({
    	global: false,
        type: "GET",
        url: "/edit_eventcomment/",
        data: { "eventcommentid": ecid.toString() }, //+1
        dataType: "json",
        success: function (json) {
        	console.log("edit event comment...");
        	console.log(json);
        	if (!(json['success'])) {
                $("#"+ecid.toString()+".eventcomment").html(json['form_html']); //+1
        	    register_edit_ec_callbacks(json['parentEventID'], ecid);
            } else {
                console.log("edit-event-comment error.");
            } 
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function register_edit_ec_callbacks(eid, ecid){ //+1
//     $('.datetimeparentdiv input').datetimepicker({'format': 'Y/m/d H:i'});
    $( "form" ).on( "submit", false );
    $('form :button.btn-warning').on('click', function(e) {
        e.preventDefault();
        get_eventcomments_for_event(eid);
    });
    $('form :submit.ajax_submit').on('click', function(e) {
        e.preventDefault();
        var formdata = $(this).parents('#'+ecid.toString()+'_id-commentForm').serializeArray(); //+1
        console.log(ecid.toString());
        formdata.push({"name" : "eventcommentid", "value": ecid.toString()}); //+1
        console.log(formdata);
        $.ajax({
    	    global: false,
            type: "POST",
            url: "/edit_eventcomment/",
            data: formdata,
            dataType: "json",
            success: function (json) {
                console.log("edit event comment...");
                console.log(json);
                if (!(json['success'])) {
//                 	console.log($("#"+ecid.toString()+".eventcomment"));
                    $("#"+ecid.toString()+".eventcomment").html(json['form_html']); //+1
            	    register_edit_ec_callbacks(json['parentEventID'], ecid);
                } else {
                    console.log("edit event comment -- success!");
                    $('#'+ecid.toString()+'.eventcomment').html(''); //+1
                    get_eventcomments_for_event(json['parentEventID']);
                }
            },
            error: function (resp) {
                console.log(resp.responseText);
            }
        });
        return false;
    });
}


function get_eventcomments_for_event(eid){
    $.ajax({
    	global: false,
        type: "GET",
        url: "/get_eventcomments_for_event/",
        data: { "eventid" : eid.toString() },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("should get "+eid.toString()+"'s event comments");
                // read into #tasklist
                $("#"+eid.toString()+".eventcommentlist").html(json['eventcomment_html']);
            } else {
                console.log("get event comment error: \n"+json);
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function remove_eventcomment(evid, ecid){
    // assume that tids are unique across all tasks!
    // walk up the tree to the first event div and get it's id!
    console.log("removing ecid: "+ecid);
    var eid = $("#"+ecid+".eventcomment").closest("div.event").attr("id");
	$.ajax({
    	global: false,
        type: "GET",
        url: "/remove_eventcomment/",
        data: { "eventid" : evid, "eventcommentid" : ecid },
        dataType: "json",
        success: function (json) {
        	console.log("remove!");
        	console.log(json);
        	if (json['success']) {
            	// SUCCESS MESSAGE
            	get_eventcomments_for_event(evid);
            } else {
                // WARNING MESSAGE
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

// taskcomments
function create_taskcomment(placeholder_tid){ // -id
    console.log("create task comment form: ", placeholder_tid.toString());  // -id
	$.ajax({
    	global: false,
        type: "GET",
        url: "/create_taskcomment/",
        data: {'taskid': (-1 * placeholder_tid) }, // back to +id
        dataType: "json",
        success: function (json) {
        	console.log("create task comment...");
        	console.log(json);
        	if (!(json['success'])) {
            	$("#"+placeholder_tid.toString()+".taskcomment").html(json['form_html']); //-id
            	console.log(json['form_html']);
            	register_tc_create_callbacks(placeholder_tid); // -id
            } else {
                console.log("create_eventcomment success error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function register_tc_create_callbacks(placeholder_tid){ // -id
//     $('.datetimeparentdiv input').datetimepicker({'format': 'Y/m/d H:i'});
    $( "form" ).on( "submit", false );
    $('form :button.btn-warning').on('click', function(e) {
        e.preventDefault();
        console.log(">>> "+placeholder_tid);
        // tid is a new comment's tcid until it's in the db and a fresh get
        console.log('#'+placeholder_tid.toString()+'.taskcomment'); // -id
        console.log( $('#'+placeholder_tid.toString()+'.taskcomment') ); // -id
        $(this).parents('#'+placeholder_tid.toString()+'.taskcomment').html('');

    });
    $('form :submit.ajax_submit').on('click', function(e) {
        e.preventDefault();
        var formdata = $('#'+placeholder_tid.toString()+'_id-commentForm').serializeArray(); // -id
        console.log(placeholder_tid.toString());
        formdata.push({"name" : "taskid", "value": (-1*placeholder_tid).toString()});
        console.log(formdata);
        $.ajax({
    	    global: false,
            type: "POST",
            url: "/create_taskcomment/",
            data: formdata,
            dataType: "json",
            success: function (json) {
                console.log("submit task comment...");
                console.log(json);
                if (!(json['success'])) {
                    $("#"+placeholder_tid.toString()+".taskcomment").html(json['form_html']); // -id
                    register_tc_create_callbacks(placeholder_tid); // -id
                } else {
                    console.log("create task comment -- success!");
                    $('#'+placeholder_tid.toString()+'.taskcomment').html('');
                    get_taskcomments_for_task(-1 * placeholder_tid);
                }
            },
            error: function (resp) {
                console.log(resp.responseText);
            }
        });
        return false;
    });
}

function edit_taskcomment(tcid){
    console.log("update task comment form: ", tcid.toString());
	$.ajax({
    	global: false,
        type: "GET",
        url: "/edit_taskcomment/",
        data: {"taskcommentid": tcid.toString()},
        dataType: "json",
        success: function (json) {
        	console.log("edit task comment...");
        	console.log(json);
        	if (!(json['success'])) {
                $("#"+tcid.toString()+".taskcomment").html(json['form_html']);            	
        	    register_tc_edit_callbacks(json['parentTaskID'], tcid);
            } else {
                console.log("edit-task-comment error.");
            } 
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}

function register_tc_edit_callbacks(tid, tcid){
//     $('.datetimeparentdiv input').datetimepicker({'format': 'Y/m/d H:i'});
    $( "form" ).on( "submit", false );
    $('form :button.btn-warning').on('click', function(e) {
        e.preventDefault();
        get_taskcomments_for_task(tid);
    });
    $('form :submit.ajax_submit').on('click', function(e) {
        e.preventDefault();
        var formdata = $(this).parents('#'+tcid.toString()+'_id-commentForm').serializeArray(); //+1
        console.log(tcid.toString());
        formdata.push({"name" : "taskcommentid", "value": tcid.toString()});
        console.log(formdata);
        $.ajax({
    	    global: false,
            type: "POST",
            url: "/edit_taskcomment/",
            data: formdata,
            dataType: "json",
            success: function (json) {
                console.log("edit task comment...");
                console.log(json);
                if (!(json['success'])) {
                    $("#"+tcid.toString()+".taskcomment").html(json['form_html']);
                    console.log(json['form_html']);
            	    register_tc_edit_callbacks(tid, tcid);
                } else {
                    console.log("edit task comment -- success!");
                    $('#'+tcid.toString()+'.taskcomment').html('');
                    get_taskcomments_for_task(tid);
                }
            },
            error: function (resp) {
                console.log(resp.responseText);
            }
        });
        return false;
    });
}


function get_taskcomments_for_task(tid){
    $.ajax({
    	global: false,
        type: "GET",
        url: "/get_taskcomments_for_task/",
        data: { "taskid" : tid.toString() },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("should get "+tid.toString()+"'s task comments");
                // read into #tasklist
                $("#"+tid.toString()+".taskcommentlist").html(json['taskcomment_html']);
                 get_assignments_for_task(tid);
            } else {
                console.log("get task comment error.");
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}


function remove_taskcomment(tid, tcid){
    // assume that tids are unique across all tasks!
    // OR walk up the tree to the first task div and get it's id ?
    console.log("removing tcid: "+tcid);
	$.ajax({
    	global: false,
        type: "GET",
        url: "/remove_taskcomment/",
        data: { "taskid" : tid, "taskcommentid" : tcid },
        dataType: "json",
        success: function (json) {
        	console.log("remove!");
        	console.log(json);
        	if (json['success']) {
                // display success message
                get_taskcomments_for_task(tid);
            } else {
                // WARNING
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}


//assignments
function assign_task_to_user(tid){
    console.log("taskid: "+tid.toString());
	$.ajax({
    	global: false,
        type: "GET",
        url: "/assign_task_to_user/",
        data: { "taskid": tid.toString(),"addflag" : "1" },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                get_task_for_taskid(tid);
            	//get_assignments_for_task(tid);
            } else {
                // WARNING error
            }
        },
        error: function (resp) {
            console.log("ERROR: "+resp.responseText);
        }
    });
    return false;
}

function unassign_task_to_user(tid){
    console.log("taskid: "+tid.toString());
	$.ajax({
    	global: false,
        type: "GET",
        url: "/assign_task_to_user/",
        data: { "taskid": tid.toString(),"addflag" : "0" },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                get_task_for_taskid(tid);
            	//get_assignments_for_task(tid);
            } else {
                // WARNING error
            }
        },
        error: function (resp) {
            console.log("ERROR: "+resp.responseText);
        }
    });
    return false;
}

function get_assignments_for_task(tid){
    $.ajax({
    	global: false,
        type: "GET",
        url: "/get_assignments_for_task/",
        data: { "taskid" : tid.toString() },
        dataType: "json",
        success: function (json) {
            console.log(json);
            if (json['success']) {
                console.log("should get "+tid.toString()+"'s task assignments");
                // read into #tasklist
                $("#"+tid.toString()+".taskassignments").html(json['assignments_html']);
            } else {
                console.log("get-task-assignments error."); // warning error
            }
        },
        error: function (resp) {
            console.log(resp.responseText);
        }
    });
}
