//

// make the login call
function perform_login(e){
  console.log(e);
  var url = "/login/"; // the script where you handle the form input.
  $.ajax({
    type: "POST",
    url: url,
    data: $("#loginform").serialize(),
    success: function(data) {
      check_for_login_success(data,"#loginreason");
    }
  });
  return false; // avoid to execute the actual submit of the form.
}


function perform_register(e){

  var url = "/register/"; // the script where you handle the form input.
  $.ajax({
    type: "POST",
    url: url,
    data: $("#registerform").serialize(),
    success: function(data) {
         check_for_login_success(data,"#registerreason");
    }
  });
  return false; // avoid to execute the actual submit of the form.
}

function check_for_login_success(data, errortarget){
    if (data.success){
        $(".input_form").css("visibility","hidden");
        perform_authctl(data);
    } else {
        $(errortarget).html(data.reason);
    }
}


// does not call perform func, just maps it!
function check_authctl(){
	$.getJSON( "/whoami/", perform_authctl );
}





function perform_authctl(data){
	console.log("performing auth control");
    if (data.uid){
        var myuid=data.uid;

        $.ajax({
            type: "GET",
            url: "lookupuid/",
            data: { "myuid":myuid },
            success: function(data) {
    			console.log("??");
                console.log(data);
                $("#hellouser").html("Hello "+data[myuid]);
            }
        });
	}
	console.log("show_auth_elements_for_userid called: " + myuid);
	show_auth_elements_for_userid(myuid); // this is in viewhelpers
}


function logout(){
	// myuid = -1;
	$.getJSON( "/logout/", perform_authctl );
}
