//

function ajaxurl(hash){
    if (hash){
        console.log (hash);
	$(".event").hide();
	$(hash+".event").show();
	}	
}



// toggle visibility of login form - what is input form's type? 
function togglelogin(){
  $(".input_form").css("visibility","hidden");
  $(".login_form").css("visibility","visible");
  return false;
}



// toggle visibility of registration form - what is registration form's type? 
function toggleregister(){
  $(".input_form").css("visibility","hidden");
  $(".register_form").css("visibility","visible");

  return false;
}




function expand_event(eid){
$("#"+eid+" > .expandable").toggleClass("expanded");
}




function expand_task(tid){
$("#"+tid+".task > .expandable").toggleClass("expanded");
}



// toggle functions - flipflop between view and edit panes

// editpane() == createpane()

// show_edit_pain
function editpane(){
    $(".pane").hide("slow");
    $("#editpane").show("slow");
}

// show_viewpane
function viewpane(){
    $(".pane").hide("slow");
    $("#viewpane").show("slow");
}

// show_edit_pain
function createpane(){
    $(".pane").hide("slow");
    $("#editpane").show("slow");
}

// show event list
function eventpane(){
    $(".pane").hide("slow");
    $("#eventlist").show("slow");
}


// make the divs for the Events - <DIV> <LABEL SPAN> <FIELD> </DIV>
function mkdiv(object,fieldname){
    return "<div id='"+ object.id + "' class= '"+ fieldname +"' >" + object[fieldname] + "</div>\n";
}

// - unused
//function mkspan(object,fieldname){
//    return "<span class= '"+ fieldname +"' >" + object[fieldname] + "</span>\n";
//}

function mklabel(cssclass,content){
    return "<span id='label" + content + "' class= '"+ cssclass +"' >" + content + "</span>\n";
}

function mkfield(object,fieldname){
    return "<div class= '"+ fieldname +"' >" + object[fieldname] + "</div>\n";
}

// var toString = Object.prototype.toString;
// isString = function (obj) {
//   return toString.call(obj) == '[object String]';
// }

// returns a day's worth of times as <option>s
function mktimesoptions(){
	var pmflag = "AM";
	var hourValues = {};
	var nonmilitary = 12;
	
	for (h=0; h<24; h++){
		if (h > 12) { 
			pmflag = "PM";
			nonmilitary = (h%12);
		} else if (h == 12) {
			pmflag = "PM";
			nonmilitary = 12;
		} else { 
			pmflag = "AM";
			nonmilitary = h;
		}
		hourValues[("" + h + ":00:00")] = ("" + nonmilitary + " " + pmflag);
	}
	console.log("starts---");
	console.log(hourValues);

	$.each(hourValues, function(key, value) {
		$('#eventTimeSelect').append($("<option/>", {
			value: key,
			text: value
		}));
	});

	console.log($('#eventTimeSelect'));
}

// returns six hours worth of duration choices in as <option>s
function mkdurationoptions(){
	var durValues = {};
	
	// every 15 mins - first hour
	for (d=1; d<4; d++){
		durValues[("" + (d * 900))] = ("" + (d * 15) + " minutes");
	}

// every 30 mins - next 5 hours
	for (dh=4; dh<24; dh++){
		if ((dh % 4) == 0) {
			durValues[("" + (dh * 900))] = ("" + Math.floor(dh / 4) + " hours");
		} else if ((dh % 4) == 2) {
			durValues[("" + (dh * 900))] = ("" + Math.floor(dh / 4) + "\u00bd hours");
		} else {
			console.log("Bad duration values!!!")
		}
	}
	
	console.log("durs---");
	console.log(durValues);

	$.each(durValues, function(key, value) {
		$('#eventDurationSelect').append($("<option/>", {
			value: key,
			text: value
		}));
	});

	console.log($('#eventTimeSelect'));
}



// roll an Event view into a div
function display(parentid, cssclass, object , whitelist){
    var fields=[];
    for (var field in whitelist){
        fields.push(mkfield(object,whitelist[field]));
    }
    
    var structure={};
    structure[cssclass]=fields.join("");
    structure["id"]=object.id;
    htmlversion=mkdiv(structure,cssclass);
	console.log("---------");
	console.log(parentid);
	console.log(htmlversion);
    $(parentid).append(htmlversion);
}


function show_auth_elements_for_userid(uid){	
	if (uid > 0){
    	$(".authonly").show();
   		$(".anononly").hide();

	} else {

    	$(".authonly").hide();
    	$(".anononly").show();
	}
}



