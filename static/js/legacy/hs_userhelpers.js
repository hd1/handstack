//

// make the login call
function perform_login(e){

  $.ajax({
    global:false,
    type: "POST",
    url:  "/login/",
    data: $("#loginform").serialize(),
    success: function(data) {
        check_for_login_success(data,"#loginreason");
    }
  });
  return false; // avoid to execute the actual submit of the form.
}


function perform_register(e){

  $.ajax({
    global:false,
    type: "POST",
    url: "/register/",
    data: $("#registerform").serialize(),
    success: function(data) {
        console.log("register success");
//         perform_login(e);
        check_for_login_success(data,"#registerreason");
    }
  });
  return false; // avoid to execute the actual submit of the form.
}

function check_for_login_success(data,errortarget){

    if (data.success){
        $(".input_form").css("visibility","hidden");
        perform_authctl(data);
    } else {
        $(errortarget).html(data.reason);
    }
    return false;
}


// does not call perform func, just maps it!
function check_authctl(){
	$.getJSON( "/whoami/", perform_authctl );
}


function perform_authctl(data){
	console.log("performing auth control");
    if (data.uid > 0){
        var myuid=data.uid;

        $.ajax({
            type: "GET",
            url: "lookupuid/",
            data: { "myuid":myuid },
            success: function(data) {
    			console.log("??");
                console.log(data);
                $("#hellouser").html("Hello "+data[myuid]);
                console.log("show_auth_elements_for_userid called: " + myuid);
            	show_auth_elements_for_userid(myuid); // this is in viewhelpers
                init_all_events();
            },
            error: function (resp) {
                console.log(resp.responseText);
            }
        });
	} else {
	    console.log("No user logged in!");
        show_auth_elements_for_userid(-1); // this is in viewhelpers
        init_all_events();
	}
}


function logout(){
	// myuid = -1;
	$.getJSON( "/logout/", perform_authctl );
}
