
// convert an address to lat/lon (relay an error if address if bogus)
function geocode(){
	   	
	adr=$("#eventAddress").val();
	console.log(adr);
	if (adr == "") {
   		adr = "400 Main Street, Buffalo, NY 14202, USA";
	} else {
		console.log("Use input Geolocation!");
		console.log(adr);
	}
	
	var geocoder = new google.maps.Geocoder();	
  	geocoder.geocode( { 'address': adr}, function(r, status) {

    	if (status == google.maps.GeocoderStatus.OK) {
      		console.log(r);
    	  	$("#eventAddress").val(r[0].formatted_address);
    	  	$("#georesult").html(r[0].geometry.location.B+"----"+r[0].geometry.location.k);
    	} else {
    	  	$("#georesult").html(status);
    	}
	});
}


function html5pos(position){

	var MyPos={latitude:position.coords.latitude,longitude: position.coords.longitude}
	glatlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
	
	var mymarker = new google.maps.Marker({
        position: glatlng ,
        map: map,
        title:"You are here",
    });

}

function dropmypin(){
	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(html5pos);
	}
}
