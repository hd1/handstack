// events={};
// events["ids"]={};
// events["sortable"]=[];

// creates the "global"/"class-level" events hash
reset_all_events();

tasks={"sortable":[]};
users={"sortable":[]};
comments={"sortable":[]};

eventsList=[
  "startdow",
  "startday",
  "startmonth",
  "starttime",
  "eventName",
  "eventDescription",
  "rsvps"
];

eventdisplaylist=[
  "startdow",
  "startday",
  "startmonth",
  "starttime",
  "eventName",
  "eventDescription",
  "eventAddress",
  "eventLocationDescription",
  "rsvps"
];

var map;

var mapready=0;
var evready=0;

// var myuid=-1;

var now= Date();
var tz= new Date();

var tzo= tz.getTimezoneOffset();
var tzh= Math.floor(tzo/60);
var tzm= tzo%60;

// this doesn't do anything to affect search anyways...
future={"startafter": now };
past={"startbefore": now };
filter=past;



// convert and FINALIZE
function prepcode(){

	adr=$("#eventAddress").val();

  	geocoder = new google.maps.Geocoder();
  	geocoder.geocode( { 'address': adr}, function(r, status) {

    	if (status == google.maps.GeocoderStatus.OK) {
      		console.log(r);
    	  	$("#eventAddress").val(r[0].formatted_address);
      		$("#georesult").html(r[0].geometry.location.B+"----"+r[0].geometry.location.k);
    	  	finalize_event({"longitude":r[0].geometry.location.B, "latitude":r[0].geometry.location.k})
	    } else {
    		$("#georesult").html(status);
    	}
  	});
    return false;
}

// *** - map events to js functions
function map_events_to_functions(){
	console.log("map_events_to_functions");
    $("#loginform").submit(perform_login);
    $("#registerform").submit(perform_register);
//     $("#create_event_form").submit(submit_create_event_form);
//     $("#eventform").submit(prepcode);
//     $("#eventdate").datepicker();
//     $("#eventdate").datepicker( "option", "dateFormat", "yy-mm-dd" );

    // make sure none of the parent objects see a click!!!
//    $("body").click(function(e) {
//        console.log("click");
//        $(".input_form").css("visibility","hidden");
//    });

    $('.input_form').click(function(e) {
        e.stopPropagation();
    });

    $('.button').click(function(e) {
        e.stopPropagation();
    });
}



// submit login data + log result to console
function submitlogin(){
    console.log( $( "#loginform" ).serialize() );
}


function chash(myhash){

    if(history.pushState) {
        history.pushState(null, null, myhash);
    } else {
        location.hash = myhash;
    }
}


function ajaxurl(hash){
    if (hash){
        hash=hash.replace(/#/, '');
        console.log (hash);
        $("#viewpane").html("");
        var events_by_ids = get_all_events_by_ids();
        display("#viewpane", "event", events_by_ids[hash] , eventdisplaylist);
        viewpane();
    }
}

function perform_event_click(theid){
	console.log("theid: " + theid);
	$("#viewpane").html("");
	var events_by_ids = get_all_events_by_ids();
	display("#viewpane", "event", events_by_ids[theid] , eventdisplaylist);
	viewpane();
	chash("#"+theid);
}

function listclick(){
	perform_event_click(this.id);
	//chash("#"+this.id);
}



function toggleBounce(marker) {
    if (marker.getAnimation() != null) {
        marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}



// leave this hear for now, called inside putmap()
function markerclick(){

    $(".event").removeClass("highlight");
    $("#"+this.eventid).addClass("highlight");

    var mpos=jQuery("#"+this.eventid);
    console.log(mpos);
    $("#eventlist").prepend(mpos);

}

// place points onto the map for locations!
// called by a number of other functions...
function putmap(){
	
	var events_by_ids = get_all_events_by_ids();
    
    for (var eventid in events_by_ids){
        events_by_ids[eventid].glatlng = new google.maps.LatLng(events_by_ids[eventid].latitude,events_by_ids[eventid].longitude);

// 		var thisicon = 'img/logo.png';
	
        var thisicon = new google.maps.MarkerImage(
        	'img/logo.png',
            new google.maps.Size(32, 32), //size
            new google.maps.Point(0,0), //origin
            new google.maps.Point(0, 0) //anchor
        );

//         var thisicon = new google.maps.MarkerImage(
//             "/getimage/?thumbnail&eventid=" + eventid , //url
//             new google.maps.Size(32, 32), //size
//             new google.maps.Point(0,0), //origin
//             new google.maps.Point(0, 0) //anchor
//         );
		
        events_by_ids[eventid].Marker=new google.maps.Marker({
            position  : events_by_ids[eventid].glatlng ,
            "eventid" : eventid,
            map       : map,
            title     : events_by_ids[eventid].eventName,
            icon      : thisicon
        });
        
        google.maps.event.addListener(events_by_ids[eventid].Marker, 'click', markerclick);

    };
    dropmypin();
    return 1;
}


function startup(){

    evready=1; // is this an events-ready flag??

    //if (mapready){putmap();}

    var all_events_sorted = get_all_events_sorted();

    for (var eventord in all_events_sorted){
        console.log(all_events_sorted[eventord]);
        display(
            "#eventlist",
            "event",
            all_events_sorted[eventord],
            eventsList
        );
    };

    $(".event").click(listclick);
    $(".event").each(function(){$(this).css("background-image","url('/getimage/?thumbnail&eventid="+this.id+"')" )});
    $("#eventlist").show("slow");

    ajaxurl(window.location.hash);
    return 1;
}

// function heartbeat() {
// 	console.log("hb");
// }
//
// function start_heartbeat() {
// 	setInterval(heartbeat, 1000);
// }

function initialize() {

    var mapOptions = {
        center: new google.maps.LatLng(42.9,-78.8), // hard coded to B-snack, will have to grab user's location
        zoom: 10
    };
    map = new google.maps.Map(document.getElementById("map_cont"), mapOptions);
    console.log("init map view");
    mapready=1;

//     start_heartbeat();
    if (evready){putmap();}


}

function populate_forms(parentid) {
	mktimesoptions();
	mkdurationoptions();
}

// call initialize_forms method once the page is ready!
$(document).ready( (function() { 
	
	console.log("READY!");
	
	console.log("CHECK auth control!");
	check_authctl();
	
	console.log("do not populate forms");
	// unroll lists of choices for dates and times
// 	populate_forms(); 
	
	console.log("map events to functions");
	map_events_to_functions();
	console.log("done mapping events to functions");
	
	// request all events and populate local data stores
	// ALSO checks who is logged in
	find_all_events(); // 2 SIDE EFFECTS!!!

}));

google.maps.event.addDomListener(window, 'load', initialize);




//specials: datetimes, latitude,lonitude, image, organizations, categories
//images: $("#event")function {$(this).css("backgroud-image","url('/getimage?eventid="+ this.id")

//dates: break up on ajax load, event.startday event.startmonth starthour etc
/////then add to evelistfeilds

//latitude, longitude: see  https://developers.google.com/maps/documentation/javascript/markers
//create marker object in events on ajax data load
//clear all markers, set markers in queryresult,

