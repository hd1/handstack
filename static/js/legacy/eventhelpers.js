//
events={};

function reset_all_events(){
	events={};
	events["ids"]={};
	events["sortable"]=[];
}

function get_events_hash(){
	return events;
}

function get_all_events_by_ids(){
	return events.ids;
}

// helper! - sort the dates
function datesort(a,b) {
    return a.startepoch - b.startepoch;
}

// actually sorts!
function get_all_events_sorted(){
	// efficiency : EVERY time the page is loaded, we are sorting. How costly is this?
    events.sortable=events.sortable.sort(datesort);
	return events.sortable;
}

// place raw json into a local hash!
// local hash object is a class-level hash passed in from caller
function loaddata(json,localobject){

    for (var obid in json) {
        localobject.ids[obid] = json[obid];
        localobject.sortable.push(json[obid]);
    }
}

// math fuckery to get date into correct format
function fixDateinfo(){
	var events_by_ids = get_all_events_by_ids();
    for (e in events_by_ids){
        events_by_ids[e]["startdt"]=new Date(events_by_ids[e].startepoch*1000)
    }
}

// request all events from database
// loadevents
function find_all_events(){
	console.log("---");
	console.log(get_events_hash());
	
    $.ajax({
        type: "GET",
        url: "/search/",
        data: filter,
        success: function (data){
        
        	var theevents = get_events_hash();
//         	console.log("+++");
//         	console.log(data['events']);
//         	console.log("+++");
//         	console.log(theevents);
            loaddata( data['events'], theevents ); // on success
                
            // HERE ARE 2 SIDE EFFECTS
            map_event_rsvps_to_userids();
            fixDateinfo();
        }
    });
}


// push a param -> val pair onto the end of the array
function prm(arr,param,val){
    var t={};
    t["name"]=param;
    t["value"]=val;
    arr.push(t);
}

// append the event id to an image to upload
function ecleanup(eventid){
    $("#imageinput").append("<input type='hidden' name='eventid' value='"+eventid+"'/>" );
    $("#imageinput").submit()

    //Do tasks
}

// finish/finalize making event
function finalize_event(coor){
	
	console.log("create new event -----------------");
    
    var newevent =$("#eventform").serializeArray();
    
    console.log("SERIALIZE");
	console.log(newevent);
    
    prm(newevent,"latitude",coor.latitude);
    prm(newevent,"longitude",coor.longitude);

    console.log(newevent);

    var d = new Date()
    var tzo=d.getTimezoneOffset();
    var tzh=Math.floor(tzo/60);
    var tzm=Math.floor((tzo/60)%1);
    prm(newevent,"eventStartTime",$("#eventdate").val()+" "+ $("#eventtime").val()+" +0000");
    console.log(newevent);

    $.ajax({
           type: "POST",
           url: "/make/",
           data: newevent,
           success: function(data)
           {
               console.log (data);
               ecleanup    (data.id);
           }
         });
    return false; // avoid to execute the actual submit of the form.

}


// convert a list of ids to a list of username strings
// helper for map_rsvps_to_userids
function convert_idlist_to_usernames(idlist){
    nlist=[];
    for (u in idlist){
        nlist[u] = users[ idlist[u] ];
    }
    return nlist;
}

// look through the list of events, see who RSVPed, and form RSVP lists
// not sure if this is a good logical location - put in userhelpers???
// WAS getusers
function map_event_rsvps_to_userids(){

	var events_by_ids = get_all_events_by_ids();
    
    for (e in events_by_ids){

        for (uid in events.ids[e].EventRSVPS){
            users[events_by_ids[e].EventRSVPS[uid]]=events_by_ids[e].EventRSVPS[uid];
        }
    }
    
    $.ajax({
        type: "GET",
        url: "/lookupuid/",
        data: users,
        success: function (data){

            users=data;
			
			// events_by_ids is composed outside this callback!!! beware?
			
            for (e in events_by_ids){
                events_by_ids[e]["rsvps"]="";
                events_by_ids[e]["rsvplist"]=convert_idlist_to_usernames(events_by_ids[e].EventRSVPS);

                for (u in events_by_ids[e].rsvplist){

                    events_by_ids[e].rsvps += mklabel("userlist",events_by_ids[e].rsvplist[u]);

                }
            }
          // call startup(): actually unroll event data into views, etc.
            startup();
        }
    });
}
