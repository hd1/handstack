// returns a day's worth of times as <option>s
function mktimesoptions(){
	var pmflag = "AM";
	var hourValues = {};
	var nonmilitary = 12;
	
	for (h=0; h<24; h++){
		if (h > 12) { 
			pmflag = "PM";
			nonmilitary = (h%12);
		} else if (h == 12) {
			pmflag = "PM";
			nonmilitary = 12;
		} else { 
			pmflag = "AM";
			nonmilitary = h;
		}
		hourValues[("" + h + ":00:00")] = ("" + nonmilitary + " " + pmflag);
	}
	console.log("starts---");
	console.log(hourValues);

	$.each(hourValues, function(key, value) {
		$('#eventTimeSelect').append($("<option/>", {
			value: key,
			text: value
		}));
	});

	console.log($('#eventTimeSelect'));
}

// returns six hours worth of duration choices in as <option>s
function mkdurationoptions(){
	var durValues = {};
	
	// every 15 mins - first hour
	for (d=1; d<4; d++){
		durValues[("" + (d * 900))] = ("" + (d * 15) + " minutes");
	}

// every 30 mins - next 5 hours
	for (dh=4; dh<24; dh++){
		if ((dh % 4) == 0) {
			durValues[("" + (dh * 900))] = ("" + Math.floor(dh / 4) + " hours");
		} else if ((dh % 4) == 2) {
			durValues[("" + (dh * 900))] = ("" + Math.floor(dh / 4) + "\u00bd hours");
		} else {
			console.log("Bad duration values!!!")
		}
	}
	
	console.log("durs---");
	console.log(durValues);

	$.each(durValues, function(key, value) {
		$('#eventDurationSelect').append($("<option/>", {
			value: key,
			text: value
		}));
	});

	console.log($('#eventTimeSelect'));
}
