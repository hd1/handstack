from django.shortcuts import render,get_object_or_404
from django.http import HttpResponseRedirect,HttpResponse,Http404
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.db import models
from django.forms.models import model_to_dict
from django.contrib.auth.models import User,Group,Permission
from django.contrib.auth import authenticate, login
import json
import datetime
import re, os
from django.contrib.auth import logout


#from notification import models as notification


from dateutil import parser

from models import Event,Location,Task,Profile,Frontline, Organization,Comment,Interest

from django.template import RequestContext, loader
import reversion

from jsonview.decorators import json_view
from crispy_forms.utils import render_crispy_form
from django.core.context_processors import csrf

import magic  #wtf python

from forms import EventForm, TaskForm, CommentForm
        
from functools import wraps
from django.views.decorators.csrf import csrf_protect

from django.template.loader import render_to_string

import urllib2
import urllib
import json

# GENERAL HELPERS




def tzoffset(lat,lon,when):
    tzo=gettzdata(lat,lon,when)
    if tzo:
# newwhen=add tzo to when, if tzo=gettzdata(lat,lon,newwhen) then return tzo else: return tzoffset(lat,lon,newwhen)
        return (tzo)
    else:
        return 0

def gettzdata(lat,lon,when):
    tzurl="https://maps.googleapis.com/maps/api/timezone/json"
    gkey="AIzaSyBfj0kAdklL0SRpXPVaOVQezmSGR8f2a2M"
    values={"location":str(lat)+","+str(lon),"timestamp":when.strftime("%s"),"key":gkey}
    tzinputs= urllib.urlencode(values)
    tzinputs="location="+str(lat)+","+str(lon)+"&timestamp="+when.strftime("%s") ## +"&key="+gkey
    stupidquery=tzurl+"?"+tzinputs
    tzdata=json.load(urllib2.urlopen(stupidquery))
    #if tzdata["status"] is "OK":
    return (tzdata["rawOffset"])
    #else:
    #    return 

def fixalltz(request):
    q=Event.objects
    data=dict()
    for thing in q.all():
        giventime=parser.parse(thing.eventStartTime)
        tzo=tzoffset(thing.latitude,thing.longitude,giventime)
        thing.locationtzseconds=tzo
        thing.eventComputedStartTime=giventime-datetime.timedelta(0,thing.locationtzseconds)
        thing.eventComputedEndTime=thing.eventComputedStartTime + datetime.timedelta(seconds=int(thing.eventDuration))
        data[thing.id]=thing.locationtzseconds
        thing.save()

    data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")

def getemails(request):
    data=dict()
    u=request.user
    permission = Permission.objects.get(codename="general_notifications")
    u.user_permissions.add(permission)
    data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")


def blockemails(request):
    data=dict()
    u=request.user       
    permission = Permission.objects.get(codename="general_notifications")
    u.user_permissions.remove(permission)
    data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")


def unsubscribe(request):
    data=dict()
    u=request.user
    if request.user.is_authenticated():
        permission = Permission.objects.get(codename="general_notifications")
        u.user_permissions.remove(permission)
        data["success"]=True
        data["result"]="You have been unsubscribed from all Handstack Notifications"
    else:
        data["result"]="You must log in. Sorry"
    template=loader.get_template('unsubscribe.html')
    context = RequestContext(request, data)
    return HttpResponse(template.render(context))








class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        try:
            return super(ComplexEncoder, obj).default(obj)
        except TypeError:
            return str(obj)


def hs_or_csrf(handler):
    @wraps(handler)
    @csrf_exempt
    def newHandler(request, *args, **kwargs):
        data=dict()
        data["uagent"]=request.META["HTTP_USER_AGENT"]
        data["success"]=True
        if "Handstack" in data["uagent"]:   
            return handler(request, *args, **kwargs)
        else:
            CSRFHandler=csrf_protect(handler)
            return CSRFHandler(request, *args, **kwargs) ##Uses CSRF
    return newHandler



'''
  ** GENERAL
'''

# GEBERAL CONTROLLER ***

# get_all_objects_json
def bigdump(request):
    data=dict()
    cdata=dict()
    for thing in Event.objects.all():
        cdata[str(thing.id)]=thing.to_dict()
    data["events"]=cdata
    data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")


'''
  ** USERS
'''

# USER CONTROLLERS



# register_user
@csrf_exempt
def userregister(request):
    email=request.POST['email']
    pword=request.POST['pword']
    uname=request.POST['username']
    data=dict();

    if len(pword) < 6:
        data["success"]=False
        data["reason"]="Password Too short (6)"
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")





    #test for already registered
    try:
        user = User.objects.get(username=uname)

        data["success"]=False
        data["reason"]="Username Exists"
        data["uid"]=str(user.id)
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

    except User.DoesNotExist:
        try:
            user = User.objects.get(email=email)

            data["success"]=False
            data["reason"]="Email is already in use"
            return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

        except User.DoesNotExist:

            newuser = User.objects.create_user(uname, email, pword)
            user = authenticate(username=uname, password=pword)
            login(request, user)
            profile=Profile()
            profile.user=user
            profile.save()
            data["success"]=True
            data["uid"]=str(user.id)
            return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

# add_user_to_event_json
@csrf_exempt
def rsvpme(request):
    data=dict()
    cdata=dict()
    q=Event.objects
    if request.user.is_authenticated():
        if 'eventid' in request.GET:
            q=q.get(id__exact=int(request.GET['eventid']))
            if not request.user.id in q.EventRSVPS.all():
                q.EventRSVPS.add(request.user)
                q.save()
        data["success"]=True
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

# remove_user_from_event_json
@csrf_exempt
def unrsvpme(request):
    data=dict()

    bdata=dict()
    cdata=dict()
    q=Event.objects
    if request.user.is_authenticated():
        if 'eventid' in request.POST:
            q=q.get(id__exact=int(request.POST['eventid']))
            q.EventRSVPS.remove(request.user)
            q.save()
        data["success"]=True
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


# add_user_to_organization_json
@csrf_exempt
def joingroup(request):
    data=dict()
    data["success"]=True
    if request.user.is_authenticated():
        o=get_object_or_404(Organization,id__exact=int(request.GET['orgid']))
        o.group.user_set.add(request.user)
    return HttpResponse(json.dumps(data),content_type="application/json")

# remove_user_from_organization_json
@csrf_exempt
def leavegroup(request):
    data=dict()
    data["success"]=True
    if request.user.is_authenticated():
        o=get_object_or_404(Organization,id__exact=int(request.GET['orgid']))
        o.group.user_set.remove(request.user)
    return HttpResponse(json.dumps(data),content_type="application/json")

# fold this into remove_user_from_organization_json
# remove_user_from_organization_json
@csrf_exempt
def kickuserfromgroup(request):
    data=dict()
    data["success"]=True
    o=get_object_or_404(Organization,id__exact=int(request.GET['orgid']))

    if request.user.is_authenticated() and request.user in o.Admins.all():
        u=get_object_or_404(User,id__exact=int(request.GET['uid']))
        o.group.user_set.remove(u)

    return HttpResponse(json.dumps(data),content_type="application/json")


# add_admin_to_organization
@csrf_exempt
def addorgadmin(request):
    data=dict()
    data["success"]=True
    if request.user.is_authenticated():
        o=get_object_or_404(Organization,id__exact=int(request.GET['orgid']))
        if request.user.is_authenticated() and request.user in o.Admins.all() or request.user==o.creator:
            o.Admins.add(request.user)
    return HttpResponse(json.dumps(data),content_type="application/json")


# remove_admin_from_organization
@csrf_exempt
def rmorgadmin(request):
    data=dict()
    data["success"]=True
    o=get_object_or_404(Organization,id__exact=int(request.GET['orgid']))

    if request.user.is_authenticated() and request.user in o.Admins.all() or request.user==o.creator:
        u=get_object_or_40(User,id__exact=int(request.GET['uid']))
        o.group.user_set.remove(u)

    return HttpResponse(json.dumps(data),content_type="application/json")

# get_events_for_user_json

@csrf_exempt
def myevents(request):
    data=dict()
    cdata=dict()
    bdata=dict()

    q=Event.objects
    if request.user.is_authenticated():
        q=request.user.eventrsvplist

        data["success"]=True
        for thing in q.all():
            cdata[str(thing.id)]=thing.to_dict()
        data["eventrsvplist"]=cdata
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")




# login_user_json
@csrf_exempt
def userlogin(request):
    pword=request.POST['pword']
    uname=request.POST['username']

    thisuser = authenticate(username=uname, password=pword)

    data=dict()
    if not thisuser or not thisuser.is_active:
        data["success"]=False
        data["reason"]="Authentication failed."
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        login(request, thisuser)
        data["success"]=True
        data["uid"]=thisuser.id
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

# USER HELPERS

# get_user_logged_in_state_json
def whoami(request):
    data=dict();
    data["useragent"]=request.META["HTTP_USER_AGENT"]
    if request.user.is_authenticated():
        data["success"]=True
        data["uid"]=request.user.id
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=True
        data["uid"]=-1
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


# is_user_admin_for_organization_bool
def isadmin(user,e):
    ##if user in e.Organization.Admins.all() or user==e.Organization.creator or
    if user.id==e.creator.id:
        return True
    return True

# get_username_for_userid_json
@csrf_exempt
def LookUpUid(request):
    data=dict()
    for field in request.GET:
        try:
            user = User.objects.get(id__exact=int(request.GET[field]))
        except User.DoesNotExist:
            data[request.GET[field]]=-1
        data[request.GET[field]]=user.username
    data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")


# get_userid_for_username_json
@csrf_exempt
def LookUpUserName(request):
    data=dict()
    try:
        user = User.objects.get(username=request.GET['username'])
    except User.DoesNotExist:
        data["uid"]=-1
        data["success"]=True
        return HttpResponse(json.dumps(data),content_type="application/json")
    data["success"]=True
    data["uid"]=user.id
    return HttpResponse(json.dumps(data),content_type="application/json")


'''
  ** ORGANIZATIONS
'''

# ORGANIZATION CONTROLLERS

# add_organizations_to_event
def doorgs(thisevent,intids):
    data=dict()
    if len(intids)==0:
        return data
    intlist = [int(x.strip()) for x in intids.split(',')]
    if len(intlist)==0:
        return data
    for intid in intlist:
        i= get_object_or_404(Organization,id__exact=intid )
        thisevent.Organizations.add(i)
    thisevent.save()
# why no response???

# update_orgnaization_json
@reversion.create_revision()
@csrf_exempt
def orgupdate(request):
    data=dict()
    cdata=dict()
    t=Organization.objects
    if request.user.is_authenticated():
        if 'orgid' in request.POST:
            o=o.get(id__exact=int(request.POST['orgid']))
    #        data["E"]=e.to_dict()

            if request.user in o.Admins.all() or request.user==o.creator:
                data["member"]="ok"
                t.lastedit=request.user
                for field in request.POST:
                    setattr(t,field, request.POST[field])
                t.save()
        data["success"]=True
        data["T"]=t.to_dict()
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


# ORGANIZATION HELPERS

'''
  ** EVENTS
'''

# EVENT CONTROLLERS

# find_events_by_location_json

def search(request):
    data=dict()
    cdata=dict()

    q=Event.objects
    latDist=1.0
    lonDist=1.0
    ringwdth=0.1
    qdata=dict()
    if 'eventid' in request.GET:
        q=q.filter(id=int(request.GET['eventid']) )
    if 'interest' in request.GET:
        q=q.filter(interests__group__name=request.GET['interest'])
    if 'latDist' in request.GET:
        latDist=float(request.GET['latDist'])
    if 'lonDist' in request.GET:
        lonDist=float(request.GET['lonDist'])

    '''
    if 'startafter' in request.GET:
        startafter=request.GET['startafter']
        datematch=match=re.match("(.*)\(", startafter)
        startafter=datematch.groups(0)[0]
        q=q.filter(eventComputedStartTime__gt=parser.parse(startafter))

    if 'startbefore' in request.GET:
        startbefore=request.GET['startbefore']
        datematch=match=re.match("(.*)\(", startbefore)
        startbefore=datematch.groups(0)[0]
        q=q.filter(eventComputedStartTime__lt=parser.parse(startbefore))
    if 'ring' in request.GET:
        if 'latitude' in request.GET:
            lat=float(request.GET['latitude'])
        if 'longitude' in request.GET:
            lon=float(request.GET['longitude'])
        ring=float(request.GET['ring'])
    '''
    if 'latitude' in request.GET:
        lat=float(request.GET['latitude'])
        qdata['latitude']=lat
        q=q.filter(latitude__range=(lat-latDist, lat+latDist))
    if 'longitude' in request.GET:
        lon=float(request.GET['longitude'])
        qdata['longitude']=lon
        q=q.filter(longitude__range=(lon-lonDist, lon+lonDist))


    for thing in q.all():
        tj=thing.to_dict()
        tj["localStartTime"]=str(thing.eventComputedStartTime+datetime.timedelta(0,thing.locationtzseconds))[:-6]
        tj["localEndTime"]=str(thing.eventComputedEndTime+datetime.timedelta(0,thing.locationtzseconds))[:-6]
        if thing.locationtzseconds>=0:
            tzs="+"
        else:
            tzs="-"
        tzh=str(abs(int(thing.locationtzseconds/3600))).zfill(2)
        tzm=str(int((thing.locationtzseconds%3600)/60)).zfill(2)


        tj["localStartTime"]+=tzs+tzh+tzm
        tj["localEndTime"]+=tzs+tzh+tzm



        cdata[str(thing.id)]=tj
    data["events"]=cdata
    #qdata['lonDist']=lonDist
    #qdata['latDist']=latDist
    #data['queryterms']=qdata
    data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

# remove_event_by_id_json
@reversion.create_revision()
@csrf_exempt
def rmevent(request):
    data=dict()
    if request.user.is_authenticated() and 'eventid' in request.GET:
        q=get_object_or_404(Event,id__exact=int(request.GET['eventid']))
        if request.user.id == q.creator.id:
            q.delete()
    data["success"]=True
    #Handle orphan tasks
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

# set_image_for_event_json

@csrf_exempt
def eventimage(request):
    data=dict()
    if "eventid" in request.POST:
        e=Event.objects
        e=e.get(id__exact=int(request.POST['eventid']))
        form = UploadFileForm(request.POST, request.FILES)
 #       data["manifest"] = request.FILES
        e.image=request.FILES['image']
        e.save()
    if "orgid" in request.POST:
        e=Organization.objects
        e=e.get(id__exact=int(request.POST['orgid']))
        form = UploadFileForm(request.POST, request.FILES)
        e.image=request.FILES['image']
        e.save()
    if "userid" in request.POST:
        e=Profile.objects
        e=e.get(user__id__exact=int(request.POST['userid']))
        form = UploadFileForm(request.POST, request.FILES)
        e.image=request.FILES['image']
        e.save()
    data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")



# get_image_for_event_json

@csrf_exempt
def geteventimage(request):
    if "eventid" in request.GET:
        e=get_object_or_404(Event,id__exact=int(request.GET['eventid']))
        if not e.image:
            raise Http404
        if "thumbnail" in request.GET:
            file=e.image_thumbnail
        else:
            file=e.image
        handle=file._get_file()
        data=handle.read()
        type=magic.from_buffer(data,mime=True)
    if "orgid" in request.GET:
        e=get_object_or_404(Organization,id__exact=int(request.GET['orgid']))
        if "thumbnail" in request.GET:
            file=e.image_thumbnail
        else:
            file=e.image
        handle=file._get_file()
        data=handle.read()
        type=magic.from_buffer(data,mime=True)
    if "userid" in request.GET:
        p=get_object_or_404(Profile,user__id__exact=int(request.GET['userid']))
        #p=get_object_or_404(Profile,user=u.id)

        if "thumbnail" in request.GET:
            file=p.image_thumbnail
        else:
            file=p.image
        handle=file._get_file()
        data=handle.read()
        type=magic.from_buffer(data,mime=True)
    return HttpResponse(data,content_type=type)



# EVENT HELPERS


'''
  ** TASKS
'''

# TASK CONTROLLERS

# get_all_tasks_json
def taskdump(request):
    data=dict()
    cdata=dict()
    for thing in Task.objects.all():
        cdata[str(thing.id)]=thing.to_dict()
    data["events"]=cdata
    data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")


# get_task_by id_json
# *** THIS METHOD HAS A SIDE EFFECT!!! --- assigning priorities
def tasksearch(request):
    data=dict()
    cdata=dict()

    q=Event.objects

    if 'eventid' in request.GET:
        q=q.filter(id=int(request.GET['eventid']) )
    for e in q.all():
        if e.tasklist:
            for thing in e.tasklist.all():
                if thing.taskPriority not in ["0","1","2","3"]:
                    thing.taskPriority="0"
                    thing.save()

                tj=thing.to_dict()
                tj["localDueTime"]=str(thing.taskComputedDuedate+datetime.timedelta(0,tj["locationtzseconds"]))[:-6]
                if tj["locationtzseconds"]>=0:
                    tzs="+"
                else:
                    tzs="-"
                tzh=str(abs(int(tj["locationtzseconds"]/3600))).zfill(2)
                tzm=str(int((tj["locationtzseconds"]%3600)/60)).zfill(2)
                tj["localDueTime"]+=tzs+tzh+tzm
                cdata[str(thing.id)]=tj

            data["tasks"]=cdata
            data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


# remove_task_by_id_json
@reversion.create_revision()
@csrf_exempt
def rmtask(request):
    data=dict()
    if request.user.is_authenticated() and "taskid" in request.GET:
        t=get_object_or_404(Task,id__exact=int(request.GET['taskid']))
        e=t.event_set.all()[0]
        if request.user.id == e.creator.id:
            t.delete()
    data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


# TASK HELPERS

'''
  ** COMMENTS
'''

# COMMENTS CONTROLLERS

# attach_comment_to_event_json
# attach_comment_to_task_json
@reversion.create_revision()
@csrf_exempt
def mkcomment(request):
    data=dict()
    if request.user.is_authenticated() and 'eventid' in request.POST:
        e=get_object_or_404(Event,id__exact=int(request.POST['eventid']))
        c=Comment()
        for field in request.POST:
            setattr(c,field, request.POST[field])
        c.author=request.user
        c.lastedit=request.user
        c.parentEvent=e
        c.save()
        data["test"]="foo"
        data["comment"]=c.to_dict()
    if request.user.is_authenticated() and 'taskid' in request.POST:
        t=get_object_or_404(Task,id__exact=int(request.POST['taskid']))
        c=TaskComment()
        for field in request.POST:
            setattr(c,field, request.POST[field])
        c.author=request.user
        c.lastedit=request.user
        c.parentEvent=t.event_set.all()[0]
        c.parentTask=t
        c.save()
        data["test"]="foo"
        data["comment"]=c.to_dict()

    #Handle orphan tasks
    data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

# get_comments_for_event_json
# get_comments_for_task_json

@reversion.create_revision()
@csrf_exempt
def comments(request):
    data=dict()
    cdata=dict()
    if 'eventid' in request.GET:
        c=Comment.objects
        e=get_object_or_404(Event,id__exact=int(request.GET['eventid']))
        c=c.filter(parentEvent=e)
        for thing in c.all():
            cdata[str(thing.id)]=thing.to_dict()
        data["comments"]=cdata
    if 'taskid' in request.GET:
        c=TaskComment.objects
        t=get_object_or_404(Task,id__exact=int(request.GET['taskid']))
        c=c.filter(parentTask=t)
        for thing in c.all():
            cdata[str(thing.id)]=thing.to_dict()
        data["comments"]=cdata
    data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")


# remove_comment_by_id_json
#   + remove_comments_for_event_json
#   + remove_comments_for_task_json

@reversion.create_revision()
@csrf_exempt
def rmcomment(request):
    data=dict()
    u=request.user
    if request.user.is_authenticated() and 'commentid' in request.GET:
        c=get_object_or_404(Comment,id__exact=int(request.GET['commentid']))
        if isadmin(u,c.parentEvent) or u==c.author:
            c.delete()
    data["success"]=True
    #Handle orphan tasks
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

# COMMENTS HELPERS



''' TEMPLATE VIEW RENDERERS '''


# get_template_for_home_html
def homeindex(request):
#    return HttpResponse("Hello, handstack.")
    template=loader.get_template('home.html')
    context = RequestContext(request, {
        'pageinfo': 5,
       })
    return HttpResponse(template.render(context))

# get_template_for_some_html
def htmlserve(request,path,filename="thankyou.html"):
#    return HttpResponse("Hello, handstack.")
    template=loader.get_template(path)
    context = RequestContext(request, {
        'pageinfo': filename,
       })
    return HttpResponse(template.render(context))

# serve base_bs templatehtml
def appserve(request):
#    return HttpResponse("Hello, handstack.")
    template=loader.get_template("base_bs.html")
    context = RequestContext(request, {
        'pageinfo': 7,
       })
    return HttpResponse(template.render(context))


# get_template_for_some_html
def cssserve(request,path,filename="thankyou.html"):
#    return HttpResponse("Hello, handstack.")
    template=loader.get_template(path)
    hsorange="#FF8826"
    hsgreen="#11B3AC"
    hsgold="#ffb912"
    loginbtn={"bg":"white","color":hsgreen,"border":"white"}
    registerbtn={"bg":hsgold,"color":"white","border":hsgold}
    volunteerbtn={"bg":hsgreen,"color":"white","border":hsgreen}
    f={"small":"0.7rem","medium":"0.8rem","large":"1rem"}
    rq={
    'pageinfo': filename,
	"headercolor":hsgold,
	"loginbtn":loginbtn,
	"registerbtn":registerbtn,
	"headerbg":hsorange,
    "volbtn":volunteerbtn,
	"f":f,
    "eventname":hsgold,
    "daterow":hsorange,
    "tasklabel":hsgreen,
    "tasktitle":hsgreen,
    "hsorange":hsorange,
    "hsgreen":hsgreen,
    "hsgold":hsgold,
#"":{"bg":,"color":,"border":}
#	"color":hs,
    }
    rq["mkeventbtn"]={"bg":hsorange,"color":"white","border":hsorange}

    context = RequestContext(request, rq)

    return HttpResponse(template.render(context),content_type="text/css")


# get_template_for_event_html
# rename to get_all_events_html
def htmlevent(request,eid):
#    return HttpResponse("Hello, handstack.")
    template=loader.get_template("eventemplate.html")
    q=Event.objects
    tdata=dict()
    cdata=dict()

    q=q.filter(id=int(eid))

    for thing in q.all():
        cdata=thing.to_dict()

    context = RequestContext(request, cdata)
    return HttpResponse(template.render(context))



















#update_event_json
@reversion.create_revision()
@csrf_exempt
def eventupdate(request):
    data=dict()
    cdata=dict()
    e=Event.objects
    u=request.user
    if u.is_authenticated():
        if 'eventid' in request.POST:
            e=e.get(id__exact=int(request.POST['eventid']))
            if isadmin(u, e):
                data["member"]="ok"
                e.lastedit=request.user
                for field in request.POST:
                    if str(field) == "interests":
                        data["intids"]=dointerests(e,request.POST[field])
                    elif str(field) == "organizations":
                        data["orgids"]=doorgs(e,request.POST[field])
                    else:
                        setattr(e,field, request.POST[field])
                e.save()
            res = e.geocode(e.eventAddress)
            e.eventAddress = res[0]
            e.latitude = res[1]
            e.longitude = res[2]
            giventime=parser.parse(request.POST['eventStartTime'])
            e.locationtzseconds=tzoffset(e.latitude,e.longitude,giventime)
            e.eventComputedStartTime= giventime-datetime.timedelta(0,e.locationtzseconds)
            e.eventComputedEndTime=e.eventComputedStartTime+ datetime.timedelta(seconds=int(request.POST['eventDuration']))
            e.save()
        data["query"]=request.POST
        data["success"]=True
        data["event"]=e.to_dict()
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")













#
@csrf_exempt
def taketask(request):
    data=dict()
    cdata=dict()
    q=Task.objects
    if request.user.is_authenticated():
        if 'taskid' in request.POST:
            q=q.get(id__exact=int(request.POST['taskid']))
            if not request.user.id in q.assignments.all():
                q.assignments.add(request.user)
                q.save()
        data["success"]=True
        data["count"]=len(q.assignments.all())
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")




@reversion.create_revision()
@csrf_exempt
def taskupdate(request):
    data=dict()
    cdata=dict()
    t=Task.objects
    if request.user.is_authenticated():
        if 'taskid' in request.POST:
            t=t.get(id__exact=int(request.POST['taskid']))
            parentevent=t.event_set.all()[0]
    #        data["E"]=e.to_dict()
    #        if request.user in parentevent.EventRSVPS.all():
            data["member"]="ok"
            t.lastedit=request.user
            for field in request.POST:
                setattr(t,field, request.POST[field])
                if field=="taskPriority":
                    if str(request.POST[field]) in ["0","1","2","3"]:
                        setattr(t,field, str(request.POST[field]))
                    else:
                        setattr(t,field, "0")
                if field=="taskDuedate":
                    t.taskComputedDuedate=parser.parse(request.POST['taskDuedate'])-datetime.timedelta(0,parentevent.locationtzseconds)
                if field=="taskCompletiondate":
                    t.CompletionTime=parser.parse(request.POST['taskCompletiondate'])-datetime.timedelta(0,parentevent.locationtzseconds)
            t.save()
        data["success"]=True
        data["T"]=t.to_dict()
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


@reversion.create_revision()
@csrf_exempt
def updatetaskstatus(request):
    data=dict()
    cdata=dict()
    t=Task.objects
    if request.user.is_authenticated():
        if 'taskid' in request.POST:
            t=t.get(id__exact=int(request.POST['taskid']))
            parentevent=t.event_set.all()[0]
    #        data["E"]=e.to_dict()
    #        if request.user in parentevent.EventRSVPS.all():
            data["member"]="ok"
            t.lastedit=request.user
            if request.POST['taskStatus'] == "True":
                setattr(t,'taskStatus', "True")
            else:
                setattr(t,'taskStatus', "False")
            t.save()
        data["success"]=True
        data["T"]=t.to_dict()
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")




@csrf_exempt
def givetask(request):
    data=dict()
    cdata=dict()
    t=Task.objects
    u=User.objects
    if request.user.is_authenticated():
        if 'taskid' in request.POST and 'uid' in request.POST:
            t=t.get(id__exact=int(request.POST['taskid']))
            u=u.get(id__exact=int(request.POST['userid']))
            e=t.event_set.all()[0]
            data["E"]=e.to_dict()

            if request.user.id == e.creator.id:
                data["owner"]="ok"
                t.assignments.add(u)
                t.save()
        data["success"]=True
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


@csrf_exempt
def droptask(request):
    data=dict()

    cdata=dict()
    q=Task.objects
    if request.user.is_authenticated():
        if 'taskid' in request.GET:
            q=q.get(id__exact=int(request.GET['taskid']))
            q.assignments.remove(request.user)
            q.save()
        data["success"]=True
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


@csrf_exempt
def mytasks(request):
    data=dict()
    cdata=dict()

    q=Task.objects
    if request.user.is_authenticated():
        q=request.user.task_set

        data["success"]=True
        for thing in q.all():
            cdata[str(thing.id)]=thing.to_dict()
        data["tasklist"]=cdata
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    else:
        data["success"]=False
        return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")













@csrf_exempt
def mkevent(request):
    data=dict()
    newevent=Event();
    newevent.latitude=0.0
    newevent.longitude=0.0
    if request.user.is_authenticated():
        newevent.creator=request.user
        giventime=parser.parse(request.POST['eventStartTime'])
        newevent.locationtzseconds=tzoffset(request.POST['latitude'],request.POST['longitude'],giventime)
        newevent.eventComputedStartTime= giventime-datetime.timedelta(0,newevent.locationtzseconds)
        newevent.eventComputedEndTime=newevent.eventComputedStartTime+ datetime.timedelta(seconds=int(float(request.POST['eventDuration'])))
        newevent.save()
        newevent.EventRSVPS.add(request.user)
        newevent.save()
        for field in request.POST:
            if str(field) == "interests":
                data["intids"]=dointerests(newevent,request.POST[field])
            elif str(field) == "organizations":
                data["orgids"]=doorgs(newevent,request.POST[field])
            else:
                setattr(newevent,field, request.POST[field])
        newevent.save()
    else:
            data["success"]=True
            return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")
    newevent.save()
    data["success"]=True
    data["id"]=str(newevent.id)
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


from django import forms

class UploadFileForm(forms.Form):
    eventid = forms.CharField(max_length=50)
    image = forms.FileField()












# create_task_for_event_json
@csrf_exempt
def mktask(request):
    data=dict()
    newtask=Task();
    newtask.taskPriority="0"
    parentevent=Event.objects.get(id__exact=int(request.POST['eventid']))
    for field in request.POST:
        setattr(newtask,field, request.POST[field])
        if field=="taskPriority":
            if str(request.POST[field]) in ["0","1","2","3"]:
                setattr(newtask,field, str(request.POST[field]))
            else:
                setattr(newtask,field, "0")
        if field=="taskDuedate":
            newtask.taskComputedDuedate=parser.parse(request.POST['taskDuedate'])-datetime.timedelta(0,parentevent.locationtzseconds)
        if field=="taskCompletiondate":
            newtask.CompletionTime=parser.parse(request.POST['taskCompletiondate'])-datetime.timedelta(0,parentevent.locationtzseconds)
    user = request.user
    newtask.creator=user
    newtask.save()
    #get event
    
    newtask.DueTime=parser.parse(request.POST['taskDuedate'])
    
    newtask.save()
    #add task to tasklist
    parentevent.tasklist.add(newtask)
    #save event
    parentevent.save()
    data["success"]=True
    data["id"]=str(newtask.id)
    data["eventName"]=str(parentevent.eventName)
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")


# create_group_json
# !!! SIDE EFFECT: CREATES ORGANIZATION
@csrf_exempt
def mkgroup(request):
    data=dict()
    user = request.user
    newgroup,created=Group.objects.get_or_create(name=request.POST['name'])
    neworg=Organization()
    neworg.group=newgroup
    newgroup.save()
    for field in request.POST:
        setattr(neworg,field, request.POST[field])
    newgroup.user_set.add(user)

    neworg.creator=request.user
    neworg.save()
    neworg.Admins.add(user)
    newgroup.save()
    neworg.save()
    data["success"]=True
    data["id"]=str(neworg.id)
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")






# get_all_organizations

@csrf_exempt
def listgroups(request):
    data=dict()
    o=Organization.objects

    for thing in o.all():
        data[str(thing.id)]=thing.to_dict()

    data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")






#  NOT SURE WHERE THIS FITS!

# get_template_for_logout_html

def logout_view(request):
    logout(request)
    data=dict();
    data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")



# FRONTLINE_CONTROLLERS - DEPRECATE!

# rename to get_all_frontline_objects_json
def frontlinedump(request):
    data=dict()
    cdata=dict()
    if 'code' in request.GET and request.GET["code"]=="frob":
        for thing in Frontline.objects.all():
            cdata[str(thing.id)]=thing.to_dict()
        data["contacts"]=cdata
        data["success"]=True
    else:
        data["success"]=True
    return HttpResponse(json.dumps(data),content_type="application/json")

@csrf_exempt
def frontline(request):
    newF=Frontline();
    for field in request.POST:
        setattr(newF,field, request.POST[field])
    #get event
    newF.save()
    template=loader.get_template('thankyou.html')
    context = RequestContext(request, {
        'pageinfo': "We appreciate your interest. ",
       })
    return HttpResponse(template.render(context))



# INTEREST CONTROLLERS --- IGNORING FOR NOW


@csrf_exempt
def listinterests(request):
    data=dict()
    o=Interest.objects

    for thing in o.all():
        data[str(thing.id)]=thing.to_dict()

    data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")

@csrf_exempt
def takeinterest(request):
    data=dict()
    data["success"]=True
    if request.user.is_authenticated():
        o=get_object_or_404(Interest,id__exact=int(request.GET['intid']))
        o.group.user_set.add(request.user)
    return HttpResponse(json.dumps(data),content_type="application/json")


@csrf_exempt
def dropinterest(request):
    data=dict()
    data["success"]=True
    if request.user.is_authenticated():
        o=get_object_or_404(Interest,id__exact=int(request.GET['intid']))
        o.group.user_set.remove(request.user)
    return HttpResponse(json.dumps(data),content_type="application/json")

def dointerests(thisevent,intids):
    data=dict()
    if len(intids)==0:
        return data

    intlist = [int(x.strip()) for x in intids.split(',')]
    if len(intlist)==0:
        return data

    data["intlist"]=intlist
    for intid in intlist:
        i= get_object_or_404(Interest,id__exact=intid )
        thisevent.interests.add(i)
        data[intid]="foo"
    thisevent.save()
    return data

@csrf_exempt
def mki(request):
    data=dict()
    interestlist=["Accessibility",
    "Animals",
    "Arts",
    "Discrimination",
    "Economy",
    "Education",
    "Environment",
    "Faith",
    "Women's Rights",
    "Government",
    "Health",
    "Human Rights",
    "Immigration",
    "Labor",
    "LGBTQIA",
    "Mental Health",
    "Prison System",
    "Race",
    "Technology",
    "Transportation",
    "Youth"]

    for intname in interestlist:
        newgroup,created=Group.objects.get_or_create(name=intname)
        newgroup.save()

        newint,created=Interest.objects.get_or_create(group=newgroup,creator=request.user)
        newint.group=newgroup

        newint.save()
        newgroup.save()
        data[intname]=intname
    data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")






def create_notifications_once(request):
    from django.utils.translation import ugettext_noop as _
    data=dict()

    notification.create_notice_type("take_task", _("Task Taken!"), _("Someone has volunteered"))
    notification.create_notice_type("drop_task", _("Task Dropped"), _("Someone realized they could not do a a task"))
    notification.create_notice_type("owner_take_task", _("Task Taken!"), _("Someone has volunteered"))
    notification.create_notice_type("owner_drop_task", _("Task Dropped"), _("Someone realized they could not do a a task"))
    notification.create_notice_type("event_rsvp", _("Event RSVP"), _("Someone is planning to attend your event"))
    notification.create_notice_type("owner_task_comment", _("Task Comment"), _("Someone is planning to attend your event"))
    notification.create_notice_type("task_comment", _("Task Comment"), _("Someone is planning to attend your event"))
    notification.create_notice_type("event_comment", _("Event Comment"), _("Someone is planning to attend your event"))
    notification.create_notice_type("task_created", _("Task created"), _("Someone created a task on your event"))
    data["success"]=True
    return HttpResponse(json.dumps(data, cls=ComplexEncoder),content_type="application/json")








#### DEFINED TWICE
PRIORITY_CHOICES = (
    ('0', '~'),
    ('1', '!'),
    ('2', '!!'),
    ('3', '!!!')
)

from PIL import Image as PImage
from os.path import join as pjoin



# serve base_bs templatehtml
@json_view
def serve_app(request):    
    cdata = dict()
    user_is_auth_flag = request.user.is_authenticated()
            
    cdata['user_logged_in'] = False
    if user_is_auth_flag:
        cdata['user_logged_in'] = True
        cdata['username'] = request.user.username
    return render(request, 
                    'base_bs.html', 
                    {'base': cdata},
                    )


# login_user_json
@json_view
def login_user(request):

    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web

    pword=request.GET.get('password')
    if pword is None:
        return { 'success' : False, 'error': "Authentication failed. Please supply a password." }
    uname=request.GET.get('username')
    if uname is None:
        return { 'success' : False, 'error': "Authentication failed. Please supply a usermame." }
    
    thisuser = authenticate(username=uname, password=pword)

    if not thisuser or not thisuser.is_active:
        return { 'success' : False, 'error': "Authentication failed." }
    else:
        login(request, thisuser)
        return { 'success' : True, 'userid': thisuser.id }


# login_user_json
@json_view
def logout_user(request):

    logout(request)
    return { 'success' : True }


# # register_user
@json_view
def register_new_user(request):

    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web

    email = request.GET.get('email')
    pword = request.GET.get('password')
    uname = request.GET.get('username')

    if len(pword) < 6:
        return { 'success' : False, 'error': "Error: password should be at least 6 characters." }

    #test for already registered
    try:
        user = User.objects.get(username=uname)
        return { 'success' : False, 'error': "Error: username already exists: "+str(user.id)+"." }
    except User.DoesNotExist:
        pass
    try:
        user = User.objects.get(email=email)
        return { 'success' : False, 'error': "Error: email already exists: "+str(email)+"." }
    except User.DoesNotExist:
        newuser = User.objects.create_user(uname, email, pword)
        user = authenticate(username=uname, password=pword)
        login(request, user)
        profile=Profile()
        profile.user=user
        profile.save()
        return { 'success': True, 'userid': user.id }



@json_view
def create_event(request):
    
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    is_post_flag = (request.method == 'POST')
    user_is_auth_flag = request.user.is_authenticated()
    
    if is_post_flag:
        form = EventForm(request.POST, request.FILES)
    else:
        if user_is_auth_flag:
            form = EventForm()
            form_html = render_crispy_form(form, context=RequestContext(request))
            return { 'success' : False, 'form_html': form_html }
        else:
            return { 'success' : False, 'perm_error' : 'Please log in!' }
    
    if is_ajax_flag and user_is_auth_flag:
        if form.is_valid():
            cd = form.cleaned_data
            newevent = Event()
            for field in cd:
                setattr(newevent,field, cd[field]) #request.POST.get(field))
            newevent.creator=request.user
            res = newevent.geocode(newevent.eventAddress)
            newevent.eventAddress = res[0]
            newevent.latitude = res[1]
            newevent.longitude = res[2]
            giventime=parser.parse(request.POST['eventStartTime'])
            newevent.locationtzseconds=tzoffset(newevent.latitude,newevent.longitude,giventime)
            newevent.eventComputedStartTime= giventime-datetime.timedelta(0,newevent.locationtzseconds)
            newevent.eventComputedEndTime=newevent.eventComputedStartTime+ datetime.timedelta(seconds=int(float(request.POST['eventDuration'])))
            newevent.save()
 
            newevent.EventRSVPS.add(request.user)
            newevent.save()

#             newevent.image = cd['image']
            newevent.save()
            return { 'success': True, 'eventid' : newevent.id }
        else:
            form_html = render_crispy_form(form, context=RequestContext(request))
            return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }

@reversion.create_revision()
@json_view
def edit_event(request):
    
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    is_post_flag = (request.method == 'POST')
    user_is_auth_flag = request.user.is_authenticated()

    eventid = None
    if is_post_flag:
        eventid = request.POST.get("eventid")
    else:
        eventid = request.GET["eventid"]

    if eventid is None:
        return { 'success' : False, 'error' : 'Must specify an event id: '+str(eventid) }
    
    theevent = Event.objects.get(id__exact=int(eventid))
    if theevent is None:
        return { 'success' : False, 'error' : 'No event with that id!' }

    if theevent.creator != request.user:
        return { 'success' : False, 'error' : 'You must be the event creator to edit!' }

    if is_post_flag and user_is_auth_flag:
        form = EventForm(request.POST, request.FILES)
    elif user_is_auth_flag:
        localtime=theevent.eventComputedStartTime+datetime.timedelta(seconds=int(theevent.locationtzseconds))
        formdata = {
            "eventName" : theevent.eventName,
            "eventDescription" : theevent.eventDescription,
            "eventAddress" : theevent.eventAddress,
            "eventStartTime" : str(localtime.strftime('%Y/%m/%d %H:%M')),
            "eventDuration" : theevent.eventDuration,
            "latitude" : theevent.latitude,
            "longitude" : theevent.longitude,
            "image" : theevent.image
        }
        form = EventForm(formdata)
        form_html = render_crispy_form(form, context=RequestContext(request))
        return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }        
    
    if is_ajax_flag and user_is_auth_flag:
        if form.is_valid():
            cd = form.cleaned_data
            for field in cd:
                if field is "image":
                    if cd["image"] != None:
                        setattr(theevent, field, cd[field])
                else:
                    setattr(theevent, field, cd[field])
#             return {'success' : False, 'error' : theevent.id}

            res = theevent.geocode(theevent.eventAddress)
            theevent.eventAddress = res[0]
            theevent.latitude = res[1]
            theevent.longitude = res[2]
            giventime=parser.parse(request.POST['eventStartTime'])
            theevent.locationtzseconds=tzoffset(theevent.latitude,theevent.longitude,giventime)
            theevent.eventComputedStartTime= giventime-datetime.timedelta(0,theevent.locationtzseconds)
            theevent.eventComputedEndTime=theevent.eventComputedStartTime+ datetime.timedelta(seconds=int(request.POST['eventDuration']))
            theevent.save()
            return { 'success': True, 'eventid' : theevent.id }
        else:
            form_html = render_crispy_form(form, context=RequestContext(request))
            return { 'success' : False, 'form_html': form_html}
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }


@json_view
@reversion.create_revision()
def remove_event(request):
    res=dict()
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()

    if is_ajax_flag and user_is_auth_flag:
        if 'eventid' in request.GET:
            q = Event.objects.filter(id=int(request.GET['eventid']))[0]
            if request.user.id == q.creator.id:
                q.delete()
                # TODO READ + DELETE TASKS FOR USERID
                return { 'success' : True, 'removed' : str(int(request.GET['eventid'])) }
            else:
                # throw creator error
                return { 'success' : False, 'error' : 'Error: You must be the event creator to delete it.' }
        else:
            return { 'success' : False, 'error' : 'Error: Event ID not in request.'  }
    else:
        return {'success' : False, 'perm_error' : 'Please log in!' }


def filter_event_data(eventdict, keylist=None):
    # default list of keys
    if keylist is None:
        keylist = ['eventName', 'eventDescription', 'eventAddress', 
    'eventLocationDescription', 'eventComputedStartTime', 'eventComputedEndTime',
     'creator', 'image', 'latitude', 'longitude', "eventDuration", 
    "endtime", "enddow", "endmonth", "endday", "endepoch",
    "starttime", "startdow", "startmonth", "startday", "startepoch", "durationhours"]
    # be sure that keys are in both...
    ilist = list(set.intersection(set(eventdict.keys()), set(keylist)))
    # return None if any keys are missing!
    if len(ilist) != len(keylist):
        return None
    res = dict()
    for ikey in ilist:
        res[ikey] = eventdict[ikey]
    # clean up datetimes
#    if res.has_key('eventComputedStartTime'):
#        res['startdow'] = res['eventComputedStartTime'].strftime("%A")
#        res['startday'] = res['eventComputedStartTime'].strftime("%d")
#        res['startmonth'] = res['eventComputedStartTime'].strftime("%B")
#        res['starttime'] = res['eventComputedStartTime'].strftime("%I:%M%p")
#        res['startepoch'] = res['eventComputedStartTime'].strftime("%s")
    return res

# login_user_json
# @csrf_exempt
# def login_user_test(request):
#     
#     # Like before, obtain the context for the user's request.
#     context = RequestContext(request)
# 
#     # If the request is a HTTP POST, try to pull out the relevant information.
#     if request.method == 'POST':
#         # Gather the username and password provided by the user.
#         # This information is obtained from the login form.
#         username = request.POST['username']
#         password = request.POST['password']
# 
#         # Use Django's machinery to attempt to see if the username/password
#         # combination is valid - a User object is returned if it is.
#         user = authenticate(username=username, password=password)
# 
#         # If we have a User object, the details are correct.
#         # If None (Python's way of representing the absence of a value), no user
#         # with matching credentials was found.
#         if user:
#             # Is the account active? It could have been disabled.
#             if user.is_active:
#                 # If the account is valid and active, we can log the user in.
#                 # We'll send the user back to the homepage.
#                 login(request, user)
#                 #return HttpResponseRedirect('base.html')
#                 return HttpResponse("Valid login details supplied: {0} {1}".format(username, password))
#             else:
#                 # An inactive account was used - no logging in!
#                 return HttpResponse("Your HandStack account is disabled.")
#         else:
#             # Bad login details were provided. So we can't log the user in.
#             print "Invalid login details: {0}, {1}".format(username, password)
#             return HttpResponse("Invalid login details supplied.")


@json_view
def create_task(request):
    
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    is_post_flag = (request.method == 'POST')
    user_is_auth_flag = request.user.is_authenticated()
    
    if is_post_flag:
        form = TaskForm(request.POST)
    elif user_is_auth_flag:
        form = TaskForm()
        form_html = render_crispy_form(form, context=RequestContext(request))
        return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }
    
    if is_ajax_flag and user_is_auth_flag:
        if form.is_valid():
            cd = form.cleaned_data
            eid = request.POST.get('eventid') #cd["eventid"];
            parentevent=Event.objects.get(id__exact=int(eid))
            if parentevent is None:
                return { 'success' : False, 'error': 'No parent event with that id?' }
            newtask = Task()
            newtask.creator=request.user            
            for field in cd:
                setattr(newtask,field, cd[field])
                # individual, specific edits
                if field=="taskPriority":                    
                    if str(request.POST.get(field)) in ["0","1","2","3"]:
                        setattr(newtask,field, str(request.POST.get(field)))
                    else:
                        setattr(newtask,field, "1")
                if field=="taskDuedate":
                    newtask.taskComputedDuedate=parser.parse(request.POST.get('taskDuedate'))-datetime.timedelta(0,parentevent.locationtzseconds)
                if field=="taskCompletiondate":
                    newtask.taskComputedCompletiondate=parser.parse(request.POST.get('taskCompletiondate'))-datetime.timedelta(0,parentevent.locationtzseconds)
            if parentevent.creator.has_perm("event.general_notifications"):
                notification.send([parentevent.creator], "task_created", {"user": request.user,"tasktitle": newtask.taskTitle, "taskdescription": newtask.taskDescription})
            newtask.save()                
            if int(parentevent.id) == int(newtask.id):
                setattr(newtask, "id", str(int(newtask.id)+1))

            #add task to tasklist
            if parentevent.tasklist is not None:
                parentevent.tasklist.add(newtask)
                parentevent.save()
            return { 'success': True, 'taskid' : newtask.id, 'parentEventID' : parentevent.id }
        else:
            form_html = render_crispy_form(form, context=RequestContext(request))
            return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }


@reversion.create_revision()
@json_view
def edit_task(request):
    
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    is_post_flag = (request.method == 'POST')
    user_is_auth_flag = request.user.is_authenticated()

    task = None
    if is_post_flag:
        taskid = request.POST.get("taskid")
    else:
        taskid = request.GET["taskid"]

    if taskid is None:
        return { 'success' : False, 'error' : 'Must specify an task id. ' }
    
    thetask = Task.objects.get(id__exact=int(taskid))
    parentevent=thetask.event_set.all()[0]

    if thetask is None:
        return { 'success' : False, 'error' : 'No task with that id!' }

    if thetask.creator != request.user:
        return { 'success' : False, 'perm_error' : 'You must be the task creator to edit!' }

    if is_post_flag:
        form = TaskForm(request.POST)
    elif user_is_auth_flag:
        formdata = {
            "taskTitle" : thetask.taskTitle,
            "taskDescription" : thetask.taskDescription,
            "taskPriority" : thetask.taskPriority,
            "taskDuedate" : str(parser.parse(thetask.taskDuedate).strftime('%Y/%m/%d %H:%M')),
        }
#         return {'success' : False, 'hi' : str(parser.parse(thetask.taskDuedate).strftime('%Y/%m/%d %H:%M'))}
        form = TaskForm(formdata)
        form_html = render_crispy_form(form, context=RequestContext(request))
        return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }

    parenteid = -1
    for ev in Event.objects.all():
        for t in ev.tasklist.all():
            if int(t.id) == int(taskid):
                parenteid = int(ev.id)
    if parenteid > 0:
        theparent = Event.objects.get(id__exact=parenteid)
    else:
        return { 'success' : False, 'error' : 'Error: parent event ' + str(parenteid) + ' not found.' }
    
    if is_ajax_flag and user_is_auth_flag:
        # return {'success' : False, 'error' : form.data}
        if form.is_valid():
            cd = form.cleaned_data
            for field in cd:
                setattr(thetask, field, cd[field])
            thetask.save()
            thetask.taskComputedDuedate=parser.parse(request.POST['taskDuedate'])-datetime.timedelta(0,theparent.locationtzseconds)
            thetask.save()
            return { 'success': True, 'taskid' : thetask.id, 'parentEventID' : parenteid }
        else:
            form_html = render_crispy_form(form, context=RequestContext(request))
            return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }


# EVENTUALLY WE'LL USE THIS
# def assign_task_to_event(request):
#     
#     is_web = True
#     is_ajax_flag = request.is_ajax() # implies is_web
#     user_is_auth_flag = request.user.is_authenticated()
# 
#     eid = request.GET.get("eventid")
#     tid = request.GET.get("taskid")
# 
#     if is_ajax_flag and user_is_auth_flag and eid is not None and tid is not None:
#     
#         #get event, attach task
#         thetask = Task.objects.get(id__exact=int(tid))
#         parentevent=Event.objects.get(id__exact=int(eid))
#         #add task to tasklist
#         if parentevent.tasklist is not None and thetask is not None:
#             parentevent.tasklist.add(thetask)
#             parentevent.save()
#             res['message'] = str(int(request.GET['eventid']))
#             return render(request, 'messages.html', {'added' : str(tid) })
#         else:
#             res['message'] = 'Error: tasklist/event mismatch'
#             return render(request, 'messages.html', {'error' : res})
#     else:
#         res['message'] = 'Error: You must be logged in.'
#         return render(request, 'messages.html', {'error' : res})

@json_view
def filter_task_data(taskdict, keylist=None):
    # default list of keys
    if keylist is None:
        keylist = ['taskTitle', 'taskPriority', 'taskStatus', 'taskDuedate']
    # be sure that keys are in both...
    ilist = list(set.intersection(set(taskdict.keys()), set(keylist)))
    # return None if any keys are missing!
    if len(ilist) != len(keylist):
        return None
    res = dict()
    for ikey in ilist:
        res[ikey] = taskdict[ikey]
    # clean up datetimes
    res['duedate'] = res['taskDuedate'].strftime("%A, %B %d, %Y %I:%M%p")
    res['dueepoch'] = res['taskDuedate'].strftime("%s")
    return res


@reversion.create_revision()
@json_view
def remove_task(request):
    res=dict()
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()

    if not user_is_auth_flag:
        return { 'success' : False, 'perm_error' : 'Error: You must be logged in to delete a task.' }
    if 'eventid' in request.GET:
        pass
    else:
        return { 'success' : False, 'error' : 'Error: You must provide an event id to delete a task.' }
    if 'taskid' in request.GET:
        pass
    else:
        return { 'success' : False, 'error' : 'Error: You must provide a task id to delete a task.' }

    if is_ajax_flag:
        evid = request.GET.get('eventid')
        taskid = request.GET.get('taskid')
        theevent = Event.objects.filter(id=int(evid))[0]
        # iterate over a list, mark the last match for deletion
        id2del = -1
        for i, task in enumerate(theevent.tasklist.all()):
            if (request.user.id == task.creator.id) and (int(taskid) == int(task.id)):
                id2del = i
        if id2del == -1:
            return {'success' : False, 'perm_error' : 'Error: User not allowed to delete this event.' }
        if request.user.id == theevent.tasklist.all()[id2del].creator.id:
            theevent.tasklist.all()[id2del].delete()
            theevent.save()
            return { 'success' : True }
        else:
            # throw creator error
            return { 'success' : False, 'perm_error' : 'Error: You must be the event creator to delete it.' }


# add_user_to_event - RSVP ME! + UNRSVP_ME
@reversion.create_revision()
@json_view
def rsvp_user_for_event(request):
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()

    res = dict()
    evid = 0
    addflag = request.GET['addflag']

    if is_ajax_flag and user_is_auth_flag:
        if 'eventid' in request.GET:
            evid = request.GET['eventid']
            theevent=Event.objects.filter(id=int(evid))
        else:
            return { 'success' : False, 'error' : 'Error: No event id given.' }
        if len(theevent) > 0:
            theRSVPs = theevent[0].EventRSVPS.all()
            if not request.user in theRSVPs and int(addflag) == 1:
                theevent[0].EventRSVPS.add(request.user)
                theevent[0].save()
        
                ######notification
            if theevent[0].creator.has_perm("event.general_notifications"):
                notification.send([theevent[0].creator], "event_rsvp", {"rsvp_user": request.user,"eventname": theevent[0].eventName})
            elif request.user in theRSVPs and int(addflag) == 0:
                theevent[0].EventRSVPS.remove(request.user)
                theevent[0].save()
            else:
                return { 'success' : False, 'error' : "Error: RSVP adjustment error." }
            theRSVPs = theevent[0].EventRSVPS.all()
            if len(theRSVPs) >= 0:
                rsvps = [[int(usr.id), str(usr.username)] for usr in theRSVPs]
                # returns a list of RSVPs
                return { 'success' : True, 'rsvps' : rsvps, 'eventid' : str(evid)}
            else:
                return { 'success' : False, 'error' : "Error: Cannot have a negative number of RSVPs!" }
        else:
            return { 'success' : False, 'error' : "Error: Event not found!" }
    else:
        return { 'success' : False, 'perm_error' : "Error: you must be logged in to RSVP!" }

@json_view 
def create_eventcomment(request):

    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    is_post_flag = (request.method == 'POST')
    user_is_auth_flag = request.user.is_authenticated()

    if is_post_flag:
        eid = request.POST.get('eventid') #cd["eventid"];  # +id
        form = CommentForm(request.POST)
    elif user_is_auth_flag:
        eid = request.GET.get('eventid') #cd["eventid"]; # +id
        form = CommentForm()
        form_html = render_crispy_form(form, context=RequestContext(request))
        form_html = "<form id=\""+str(-1*int(eid))+"_id-commentForm\" class=\"form-horizontal\" method=\"post\">"+form_html+"</form>" # -id
        return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }
    
    if is_ajax_flag and user_is_auth_flag:
        if form.is_valid():
            cd = form.cleaned_data

            theevent = Event.objects.get(id__exact=int(eid))
            if theevent is None:
                return { 'success' : False, 'error' : 'No event by that id.' }
            c=Comment()
            for field in cd:
                setattr(c, field, cd[field])
            c.author = request.user
            c.lastedit = request.user
            c.parentEvent = theevent
            for fellowRSVP in theevent.EventRSVPS.all():
                if fellowRSVP.has_perm("event.general_notifications"):
                    notification.send([fellowRSVP], "event_comment", { "user": request.user,"eventname": theevent.eventName, "body": c.body })
            c.save()
            return { 'success': True, 'ecid' : c.id, 'parentEventID' : theevent.id }
        else:
            form_html = render_crispy_form(form, context=RequestContext(request))
            form_html = "<form id=\""+str(-1*int(eid))+"_id-commentForm\" class=\"form-horizontal\" method=\"post\">"+form_html+"</form>"# -id
            return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }


@reversion.create_revision() 
@json_view
def edit_eventcomment(request):
    
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    is_post_flag = (request.method == 'POST')
    user_is_auth_flag = request.user.is_authenticated()
    
    if is_post_flag:
        ecid = request.POST.get('eventcommentid')
    else:
        ecid = request.GET['eventcommentid']
    if ecid is None:
        return { 'success' : False, 'error' : 'Must specify an event comment id!'}
    thecomment = Comment.objects.get(id__exact=int(ecid))
    if thecomment is None:
        return { 'success' : False, 'error' : 'No exising event comment with that id!'}
    
    if is_ajax_flag and user_is_auth_flag:
        if is_post_flag:
            form = CommentForm(request.POST)
        elif thecomment.author == request.user:
            form = CommentForm({'body': thecomment.body})
            form_html = render_crispy_form(form, context=RequestContext(request))
            form_html = "<form id=\""+str(ecid)+"_id-commentForm\" class=\"form-horizontal\" method=\"post\">"+form_html+"</form>" # -id
            return { 'success' : False, 'form_html': form_html, 'parentEventID' : thecomment.parentEvent.id }
        else:
            return { 'success' : False, 'error' : 'You must be the comment creator to edit!' }
        
        if form.is_valid():
            cd = form.cleaned_data
            thecomment.body = cd['body']
            thecomment.save()
            return { 'success': True, 'ecid' : thecomment.id, 'parentEventID' : thecomment.parentEvent.id }
        else:
            form_html = render_crispy_form(form, context=RequestContext(request))
            form_html = "<form id=\""+str(ecid)+"_id-commentForm\" class=\"form-horizontal\" method=\"post\">"+form_html+"</form>" # -id
            return { 'success' : False, 'form_html': form_html, 'parentEventID' : thecomment.parentEvent.id }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }

@reversion.create_revision() 
@json_view
def remove_eventcomment(request):
    res=dict()
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()

    if not user_is_auth_flag:
        return { 'success': False, 'error' : 'Error: You must be logged in to delete a comment.' }
    if 'eventid' in request.GET:
        pass
    else:
        return { 'success': False, 'error' : 'Error: You must provide an event id to delete an event comment.' }
    if 'eventcommentid' in request.GET:
        pass
    else:
        return { 'success': False, 'error' : 'Error: You must provide a event comment id to delete an event comment.' }

    if is_ajax_flag and user_is_auth_flag:
        evid = request.GET.get('eventid')
        ecid = request.GET.get('eventcommentid')
        theevent = Event.objects.all().filter(id=int(evid))[0]
        c=Comment.objects.filter(parentEvent_id=evid, id=ecid)[0] # be greedy
        # iterate over a list, mark the last match for deletion
        if c is None:
            return { 'success': False, 'error' : 'Error: No event comments found for that event.' }
        if request.user.id == c.author_id:
            c.delete()
            return { 'success' : True, 'eventid' : evid }
        else:
            # throw creator error
            return { 'success' : False, 'error' : 'Error: You must be the event comment creator to delete it.' }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }


@json_view
def create_taskcomment(request):

    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    is_post_flag = (request.method == 'POST')
    user_is_auth_flag = request.user.is_authenticated()

    if is_post_flag:
        tid = request.POST.get('taskid') #cd["eventid"];  # +id
        form = CommentForm(request.POST)
    elif user_is_auth_flag:
        tid = request.GET.get('taskid') #cd["eventid"]; # +id
        form = CommentForm()
        form_html = render_crispy_form(form, context=RequestContext(request))
        form_html = "<form id=\""+str(-1*int(tid))+"_id-commentForm\" class=\"form-horizontal\" method=\"post\">"+form_html+"</form>" # -id
        return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }
    
    if is_ajax_flag and user_is_auth_flag:
        if form.is_valid():
            cd = form.cleaned_data
            thetask = Task.objects.get(id__exact=int(tid))
            if thetask is None:
                return { 'success': False, 'error': 'Error no task with that taskid.' }
            c=Comment()
            for field in cd:
                setattr(c, field, cd[field])
            c.author = request.user
            c.lastedit = request.user
            c.parentTask = thetask
            e=thetask.event_set.all()[0]
            if e.creator.user.has_perm("event.general_notifications"):
                notification.send([e.creator], "task_comment", {"user": request.user,"tasktitle": thetask.taskTitle, "body": c.body})
            for fellowtake in thetask.assignments.all():
                if fellowtake.has_perm("event.general_notifications"):
                    notification.send([fellowtake], "task_comment", {"user": request.user,"tasktitle": thetask.taskTitle, "body": c.body})
            c.save()
            return { 'success': True, 'tcid' : c.id, 'parentTaskID' : thetask.id }
        else:
            form_html = render_crispy_form(form, context=RequestContext(request))
            form_html = "<form id=\""+str(-1*int(tid))+"_id-commentForm\" class=\"form-horizontal\" method=\"post\">"+form_html+"</form>" # -id
            return { 'success' : False, 'form_html': form_html }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }


@reversion.create_revision() 
@json_view
def edit_taskcomment(request):
    
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    is_post_flag = (request.method == 'POST')
    user_is_auth_flag = request.user.is_authenticated()
    
    if is_post_flag:
        tcid = request.POST.get('taskcommentid')
    else:
        tcid = request.GET['taskcommentid']
    if tcid is None:
        return { 'success' : False, 'error' : 'Must specify an task comment id!' }
    thecomment = Comment.objects.get(id__exact=int(tcid))
    if thecomment is None:
        return { 'success' : False, 'error' : 'No task comment with that id!' }
    
    if is_post_flag:
        form = CommentForm(request.POST)
    elif user_is_auth_flag:
        form = CommentForm({'body': thecomment.body})
        form_html = render_crispy_form(form, context=RequestContext(request))
        form_html = "<form id=\""+str(tcid)+"_id-commentForm\" class=\"form-horizontal\" method=\"post\">"+form_html+"</form>" # -id
        return { 'success' : False, 'form_html': form_html, 'parentTaskID' : thecomment.parentTask.id }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }
    
    if thecomment.author != request.user:
        return { 'success' : False, 'perm_error' : 'You must be the comment creator to edit!' }

    if is_ajax_flag and user_is_auth_flag:
        if form.is_valid():
            cd = form.cleaned_data
            thecomment.body = cd['body']
            thecomment.save()
            return { 'success': True, 'tcid' : thecomment.id, 'parentTaskID' : thecomment.parentTask.id }
        else:
            form_html = render_crispy_form(form, context=RequestContext(request))
            form_html = "<form id=\""+str(tcid)+"_id-commentForm\" class=\"form-horizontal\" method=\"post\">"+form_html+"</form>" # -id
            return { 'success' : False, 'form_html': form_html, 'parentTaskID' : thecomment.parentTask.id }
    else:
        return { 'success' : False, 'perm_error' : 'Please log in!' }

@reversion.create_revision() 
@json_view
def remove_taskcomment(request):
    res=dict()
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()

    if not user_is_auth_flag:
        return { 'success' : False, 'perm_error' : 'Error: You must be logged in to delete a comment.' }
    if 'taskid' in request.GET:
        pass
    else:
        return { 'success' : False, 'error' : 'Error: You must provide an task id to delete an task comment.' }
    if 'taskcommentid' in request.GET:
        pass
    else:
        return { 'success' : False, 'error' : 'Error: You must provide a task comment id to delete an task comment.' }

    if is_ajax_flag:
        tskid = request.GET.get('taskid')
        tcid = request.GET.get('taskcommentid')
        thetask = Task.objects.all().filter(id=int(tskid))[0]
        c=Comment.objects.filter(parentTask_id=tskid, id=tcid)[0] # be greedy
        # iterate over a list, mark the last match for deletion
        if c is None:
            return { 'success' : False, 'error' : 'Error: No task comments found for that task.' }
        if request.user.id == c.author_id:
            c.delete()
            return { 'success' : True, 'taskid': tskid }
        else:
            return { 'success' : False, 'perm_error' : 'Error: You must be the task comment creator to delete it.' }


@reversion.create_revision() 
@json_view
def assign_task_to_user(request):
    
    res=dict()
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()
    addflag = int(request.GET['addflag'])
    if 'taskid' in request.GET:
        taskid = request.GET['taskid']
    else:
        return { 'success' : False, 'error' : 'Error: You must provide a task ID.' }

    thetask=Task.objects.get(id__exact=int(taskid))
    alreadytaken = request.user in thetask.assignments.all()
    if user_is_auth_flag:
        if addflag > 0 and alreadytaken == False:
            thetask.assignments.add(request.user)
            thetask.save()
            ######notification
            e=thetask.event_set.all()[0]
            if e.creator.has_perm("event.general_notifications"):
                notification.send([e.creator], "owner_take_task", {"user": request.user,"tasktitle": thetask.taskTitle})
            for fellowtake in thetask.assignments.all():
                if fellowtake.has_perm("event.general_notifications"):
                    notification.send([fellowtake], "take_task", {"user": request.user,"tasktitle": thetask.taskTitle})
            

        elif addflag < 1 and alreadytaken:
            thetask.assignments.remove(request.user)
            thetask.save()
            ######notification
            e=thetask.event_set.all()[0]
            if e.creator.has_perm("event.general_notifications"):
                notification.send([e.creator], "owner_drop_task", {"user": request.user,"tasktitle": thetask.taskTitle})
            for fellowtake in thetask.assignments.all():
                if fellowtake.has_perm("event.general_notifications"):
                    notification.send([fellowtake], "drop_task", {"user": request.user,"tasktitle": thetask.taskTitle})
        return { 'success' : True, 'taskid': taskid, 'count': len(thetask.assignments.all()) }
    else:
        return { 'success' : False, 'perm_error' : 'Error: Please log in to volunteer for a task.' }

def get_active_eventids(request):
    
    idlist = []
    cdata = {}
    data = {}
    is_ajax_flag = request.is_ajax() # implies is_web
    get_divs_flag = request.GET['divs_flag']
        
    if is_ajax_flag:
        for event in Event.objects.all():
            idlist += [int(event.id)]
    if get_divs_flag:
        for event in Event.objects.all():
            cdata[str(event.id)] = filter_event_data(event.to_dict(), ['eventName']) # use default list
            if event.creator.id == request.user.id:
                cdata[str(event.id)]['user_can_edit'] = True
                cdata[str(event.id)]['user_can_delete'] = True
        data = cdata # return plural
        html = render_to_string('event_list_bs.html', {'events' : data})
        return HttpResponse(json.dumps({'success': True, 'active_event_list': idlist, 'event_html' : html}))

def get_active_taskids(request):
    
    data = []
    is_ajax_flag = True #request.is_ajax() # implies is_web
    
    for task in Task.objects.all():
        data += [int(task.id)]
    
    return HttpResponse(json.dumps(data)) 

def get_events(request):
    
    data = dict()
    cdata = dict()
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()
    
    if True:
        if 'eventid' in request.GET:
            evid = request.GET['eventid']
            events=Event.objects.filter(id=int(evid))
            # TODO : HANDLE cases where len(res) != 1? corrupted db?
            event = events[0]
            cdata[str(evid)] = filter_event_data(event.to_dict()) # use default list
            # just in case these are going to flash up to the screen in a slow-connection situation?
            cdata[str(evid)]['user_logged_in'] = False
            if user_is_auth_flag:
                cdata[str(evid)]['user_logged_in'] = True

            if event.creator.id == request.user.id:
                cdata[str(event.id)]['user_can_edit'] = True
                cdata[str(event.id)]['user_can_delete'] = True
            data = cdata
            html = render_to_string('event_list_bs.html', {'events' : data})            
        else:
            return {'success' : False, 'error' : 'Error: you must stipulate an event id.' }
        return HttpResponse(json.dumps({'success': True, 'event_html': html}))

def get_rsvps_for_event(request):

    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()
    res=dict()

    if is_ajax_flag and 'eventid' in request.GET:
        evid = request.GET['eventid']
        res["eventid"] = evid
        theevent=Event.objects.filter(id=int(evid))
#         return HttpResponse(json.dumps({'success':False, 'error': str(theevent.id)}))
        # theevent is a list - how to handle if list is len > 1 ???
        
        if len(theevent) > 0:
            theRSVPs = theevent[0].EventRSVPS.all()
            num = len(theRSVPs)
            res["num"] = num
            res['user_logged_in'] = False
            if user_is_auth_flag:
                res['user_logged_in'] = True
            if request.user.id in [usr.id for usr in theRSVPs]:
                res['user_is_rsvped'] = True
            if num > 0:
                rsvps = [str(usr.username) for usr in theRSVPs]
                if num > 5:
                    andnum = num - 5
                    rsvps = ", ".join(rsvps[:5])
                    andstring = (" and "+str(andnum)+" others are attending.")
                    rsvps += andstring
                elif num == 1:
                    rsvps = (rsvps[0] + " is attending.")
                else:
                    rsvps = (", ".join(rsvps[:-1])) + " and " + rsvps[-1] + " are attending."
                # returns a String! of RSVPs
            else:
                rsvps = "There are no RSVPs yet."            
            res["rstring"] = rsvps
            html = render_to_string('rsvp_list.html', {'rsvps' : res})
            return HttpResponse(json.dumps({'success': True, 'rsvp_html': html}))
            
        else:
            return HttpResponse(json.dumps({'success': False, 'error': 'Error: no matching event ID found!'}))
    else:
        return HttpResponse(json.dumps({'success': False, 'error': 'Error: no event ID specified!'}))

def get_tasks_for_event(request):
    
    data = {}
    cdata = {}
    taskids = []
    user_is_auth_flag = request.user.is_authenticated()

    theevent = None
    
    if 'eventid' in request.GET:
        evid = request.GET['eventid']
        # or is .get(id__exact=int(evid)) better
        theevent=Event.objects.filter(id=int(evid))[0]
        # TODO : HANDLE cases where len(theevent) != 1? corrupted db?
        thetasks = theevent.tasklist.all()
        for task in thetasks:
            cdata[str(task.id)]=task.to_dict()
            cdata[str(task.id)]['taskPriorityInt'] = PRIORITY_CHOICES[int(cdata[str(task.id)]['taskPriority'])][0]
            cdata[str(task.id)]['taskPriority'] = PRIORITY_CHOICES[int(cdata[str(task.id)]['taskPriority'])][1]
            cdata[str(task.id)]['dueepoch'] = cdata[str(task.id)]['taskComputedDuedate'].strftime("%s")
            cdata[str(task.id)]['user_can_edit'] = False
            cdata[str(task.id)]['user_can_delete'] = False

            cdata[str(task.id)]['user_logged_in'] = user_is_auth_flag

            if request.user.id == task.creator.id:
                cdata[str(task.id)]['user_can_edit'] = True
                cdata[str(task.id)]['user_can_delete'] = True
            taskids += [str(task.id)]
            cdata[str(task.id)]["count"] = len(task.assignments.all())
            if task.taskStatus=="True":
                cdata[str(task.id)]['checked'] = "checked"
            else:
                cdata[str(task.id)]['checked'] = ""

            assignees = list(task.assignments.all())
            assignees = ", ".join([user.username for user in assignees])
            cdata[str(task.id)]["astring"] = assignees
            if request.user.id in [usr.id for usr in task.assignments.all()]:
                cdata[str(task.id)]['user_is_assigned'] = True
            else:
                cdata[str(task.id)]['user_is_assigned'] = False
        data = cdata
        html = render_to_string('task_list_bs.html', {'tasks' : data})
        return HttpResponse(json.dumps({'success': True, 'task_html': html, 'taskids' : taskids, 'user' : str(request.user)}))
    else:
        return HttpResponse(json.dumps({'success': False, 'error': 'Error: no matching event ID found!'}))

def get_task_for_taskid(request):
    
    data = {}
    cdata = {}
    taskids = []
    user_is_auth_flag = request.user.is_authenticated()
    if 'taskid' in request.GET:
        tid = request.GET['taskid']
        thetasks = Task.objects.filter(id=int(tid))
        if len(thetasks) > 0:
            task = thetasks[0]
            cdata[str(task.id)]=task.to_dict()
            cdata[str(task.id)]['user_logged_in'] = user_is_auth_flag

            cdata[str(task.id)]['taskPriority'] = PRIORITY_CHOICES[int(cdata[str(task.id)]['taskPriority'])][1]
            cdata[str(task.id)]['dueepoch'] = cdata[str(task.id)]['taskComputedDuedate'].strftime("%s")
            cdata[str(task.id)]['user_can_edit'] = False
            cdata[str(task.id)]['user_can_delete'] = False
            cdata[str(task.id)]["count"]=len(task.assignments.all())
            if task.taskStatus=="True":
                cdata[str(task.id)]['checked'] = "checked"
            else:
                cdata[str(task.id)]['checked'] = ""
            if request.user.id == task.creator.id:
                cdata[str(task.id)]['user_can_edit'] = True
                cdata[str(task.id)]['user_can_delete'] = True
            taskids += [str(task.id)]
            if request.user in task.assignments.all():
                cdata[str(task.id)]['user_is_assigned'] = True
            else:
                cdata[str(task.id)]['user_is_assigned'] = False
            data = cdata
        html = render_to_string('task_list_bs.html', {'tasks' : data})
        return HttpResponse(json.dumps({'success': True, 'task_html': html, 'taskids' : taskids, 'user' : str(request.user)}))
    else:
        return HttpResponse(json.dumps({'success': False, 'error': 'Error: no matching task ID found!'}))

def get_assignments_for_task(request):
    
    res=dict()
    is_web = True
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()

    if is_ajax_flag and 'taskid' in request.GET:
        taskid = request.GET['taskid']
        thetask=Task.objects.get(id__exact=int(taskid))
        res["num"] = len(thetask.assignments.all())        
        res["taskid"] = taskid
        assignees = list(thetask.assignments.all())
        assignees = ", ".join([user.username for user in assignees])
        res["astring"] = assignees
        if request.user.id in [usr.id for usr in thetask.assignments.all()]:
            res['user_is_assigned'] = True
        else:
            res['user_is_assigned'] = False
        res['user_logged_in'] = False
        if user_is_auth_flag:
            res['user_logged_in'] = True
        html = render_to_string('taskassignment_list.html', {'assignments' : res})
        return HttpResponse(json.dumps({'success':True, 'assignments_html': html}))
    else:
        return HttpResponse(json.dumps({'success':False, 'error': 'Error: no matching task ID found!'}))

def get_eventcomments_for_event(request):
    
    data = dict()
    cdata = dict()
    is_ajax_flag = request.is_ajax() # implies is_web
    user_is_auth_flag = request.user.is_authenticated()

    if is_ajax_flag and 'eventid' in request.GET:
        evid = request.GET['eventid']
        theevent = Event.objects.filter(id=int(evid))[0]
        # TODO : HANDLE cases where len(theevent) != 1? corrupted db?
        allcommentsforevent = Comment.objects.filter(parentEvent=theevent) #parentEvent_exact?
        for comment in allcommentsforevent.all():
            cdata[str(comment.id)]=comment.to_dict()
            usr = User.objects.get(id__exact=int(cdata[str(comment.id)]["author"]))
            #return {'success':False, 'error': 'commenter: '+str(commenter.id)+' userid: '+str(request.user.id)}
            cdata[str(comment.id)]["authorName"] = usr.username
            # cdata[str(comment.id)]['user_logged_in'] = False
            # if user_is_auth_flag:
            #     cdata[str(comment.id)]['user_logged_in'] = True
            if comment.author.id == request.user.id:
                cdata[str(comment.id)]['user_can_edit'] = True
                cdata[str(comment.id)]['user_can_delete'] = True
        data = cdata
        eventcomment_html = render_to_string('eventcomment_list.html', {'eventcomments' : data})
        return HttpResponse(json.dumps({'success': True, 'eventcomment_html': eventcomment_html}))
    else:
        return {'success': False, 'error': 'Error: no matching event ID found!'}

def get_taskcomments_for_task(request):
    
    data = dict()
    cdata = dict()
    is_ajax_flag = request.is_ajax() # implies is_web
    
    if is_ajax_flag and 'taskid' in request.GET:
        tid = request.GET['taskid']
        thetask = Task.objects.filter(id=int(tid))[0]
        allcommentsfortask = Comment.objects.filter(parentTask=thetask)
        for comment in allcommentsfortask.all():
            cdata[str(comment.id)]=comment.to_dict()
            usr = User.objects.get(id__exact=int(cdata[str(comment.id)]["author"]))
            cdata[str(comment.id)]["authorName"] = usr.username
            # cdata[str(comment.id)]['user_logged_in'] = False
            # if user_is_auth_flag:
            #     cdata[str(comment.id)]['user_logged_in'] = True
            if comment.author.id == request.user.id:
                cdata[str(comment.id)]['user_can_edit'] = True
                cdata[str(comment.id)]['user_can_delete'] = True
        data = cdata
        taskcomment_html = render_to_string('taskcomment_list.html', {'taskcomments' : data})
        return HttpResponse(json.dumps({'success': True, 'taskcomment_html': taskcomment_html}))
    else:
        return {'success': False, 'error': 'Error: no matching task ID found!'}




