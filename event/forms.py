from django import forms
from django.core import validators

import datetime, time

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Button, Hidden, HTML, Div

HOTEL_LAFAYETTE_ADDRESS = '391 Washington St, Buffalo, NY 14203'
HOTEL_LAFAYETTE_COORDINATES = ['42.88492','-78.872789']

STARTTIME_CHOICES = tuple([(('%i:00:00' % t), ('%i%s' % (((t % 12) if ((t % 12) > 0) else 12), ('AM' if ((t/12) < 1) else 'PM') ))) for t in range(24)])

DURATION_CHOICES = (
    ('900', '15 minutes'),
    ('1800', '30 minutes'),
    ('2700', '45 minutes'),
    ('3600', '1 hour'),
    ('5400', '1 1/2 hours'),
    ('7200', '2 hours'),
    ('9000', '2 1/2 hours'),
    ('10800', '3 hours'),
    ('12600', '3 1/2 hours'),
    ('14400', '4 hours'),
    ('16200', '4 1/2 hours'),
    ('18000', '5 hours'),
    ('21600', '6 hours')
)


from django.core.validators import RegexValidator



class EventForm(forms.Form):

# widgets first
#     calendar_widget = forms.widgets.DateInput(attrs={'class': 'date-pick'}, format='%Y-%m-%d')
#     time_widget = forms.widgets.TimeInput(attrs={'class': 'time-pick'})

    eventName = forms.CharField(
        widget = forms.TextInput,
        label = 'Event Title',
        initial = 'My event',
        help_text = 'Required. Pick something recognizable and memorable.',
        validators = [
            validators.MinLengthValidator(2),
            validators.MaxLengthValidator(64)
        ],
        error_messages = {
            'required' : 'A title is required. Please enter your title.',
            'min_length' : 'Title must be greater than 2 characters',
            'max_length' : 'Title must be less than 64 characters.'
        }
    )
    eventDescription = forms.CharField(
        label = 'Event Description',
        widget = forms.Textarea,
        initial = 'All about the event.',
        help_text = 'Describe your event in a few sentences.',
        validators = [
            validators.MinLengthValidator(0),
            validators.MaxLengthValidator(5000)
        ],
        error_messages = {
            'max_length' : 'Description must be less than 5000 characters.'
        }
    )
    eventAddress = forms.CharField(
        required = False,
        label = 'Event Address',
        widget = forms.TextInput,       
        initial = "Buffalo, NY 14201",
        help_text = 'Type the postal address.',
        validators = [
            validators.MinLengthValidator(0),
            validators.MaxLengthValidator(200)
        ],
        error_messages = {
            'max_length' : 'Address entry must be less than 200 characters. Please try again.'
        }
    )
    latitude = forms.CharField(
        label = 'Event Latitude Coordinate',
        required = False,
        widget = forms.TextInput,       
        initial = HOTEL_LAFAYETTE_COORDINATES[0],
        # help_text = 'Required. Type the latitude coordinate.',

        validators = [
            validators.MinLengthValidator(1),
            validators.MaxLengthValidator(12)
        ],
        error_messages = {
            # 'required' : 'A latitude coordinate is required. Please enter one.',
            'min_length' : 'Latitude coordinate must at least 1 character. Please try again.',
            'max_length' : 'Latitude coordinate must be less than 12 characters. Please try again.'
        }
    )       
    longitude = forms.CharField(
        label = 'Event Longitude Coordinate',
        required = False,
        widget = forms.TextInput,       
        initial = HOTEL_LAFAYETTE_COORDINATES[1],
        # help_text = 'Required. Type the longitude coordinate.',
        validators = [
            validators.MinLengthValidator(1),
            validators.MaxLengthValidator(12)
        ],
        error_messages = {
            # 'required' : 'A longitude coordinate is required. Please enter one.',
            'min_length' : 'Longitude coordinate must at least 1 character. Please try again.',
            'max_length' : 'Longitude coordinate must be less than 12 characters. Please try again.'
        }
    )       
    # EDT HACK!!!
    #TODO : fix things to use proper time zones!!!
    now_hour_plus_one = ((time.localtime().tm_hour - 4 + 1) % 24)
    now_pm = ('AM' if (now_hour_plus_one < 12) else 'PM')
    tdy = datetime.date.today()
    
    eventStartTime = forms.DateTimeField(
        label = 'Event Start Date + Time',
        widget = forms.TextInput,
        initial = 'date', #('%04i/%02i/%02i %02i:00' % (tdy.year, tdy.month, tdy.day, now_hour_plus_one)), # today + top of next hour
        help_text = 'Required. At what time does the event start?',
        input_formats=['%Y/%m/%d %H:%M'],
        # validators=[RegexValidator('.*', 'boo')],
        # validators=[RegexValidator('^\d{4}/\d{2}/\d{2} \d{2}:\d{2} [-|+]\d{4}$', message='Invalid date/time format. Please enter your start date/time.')],
        error_messages = {
            'required' : 'A start date + time is required. Please enter your start date + time.',
            'invalid' : 'Invalid time format. Please enter your start date + time.'
        }
    )
    eventDuration = forms.ChoiceField(
        choices=DURATION_CHOICES,
        label = 'Event Duration',
        widget = forms.Select,
        initial = DURATION_CHOICES[3][0], # 1 hour is the default!
        help_text = 'Required. How long is your event?',
#       validators = [], # are they implied???
        error_messages = {
            'required' : 'A duration is required. Please select your duration.',
            'invalid_choice' : 'Invalid duration.'
        }
    )
    image = forms.FileField(
        label='Select a image for your event.',
#         initial=True,
        help_text='Optional, max. 2.5 MB.',
        required=False
    )

    helper = FormHelper()
    helper.form_id = 'id-eventForm'
    helper.form_tag = True
    helper.form_class = 'form-horizontal'
    helper.field_template = 'hs_field_with_buttons.html'
    helper.label_class = 'obliviate'
#    helper.field_class = 'col-md-2'
    helper.layout = Layout(
        Div('latitude', css_class="hideloc"),
        Div('longitude', css_class="hideloc"),
        Div(
            Div('eventName', css_class="row"),
            Div('eventDescription', css_class="row"),
            Div('eventAddress', css_class="row"),
            Div(
                Div(
                    Div('eventStartTime', css_class="datetimeparentdiv col-md-6  col-xs-6 "),
                        Div('eventDuration', css_class="durationselect col-md-6 col-xs-6  ")
                        ), 
                css_class="row"
            ), 
            css_class="col-md-6"
        ),
        Div('image', css_class="col-md-6"),
        Div(
            Div(
                Button('Dismiss', 'dismiss', css_class='btn btn-warning btn-xs'),
                Submit('Update', 'update', css_class='ajax_submit btn btn-xs'), 
                css_class="col-md-12 text-right"
            ), 
            css_class="row"
        )
    )
    
#     def __init__(self, data=None, *args, **kwargs):
#         super(EventForm, self).__init__(data=None, **kwargs)
#         if 'image' in self.data:
#             if self.data['image']!='':
#                 self.fields['image'].initial=self.data['image']



#########THIS IS DEFINED TWICE I THINK##########
PRIORITY_CHOICES = (
    ('0', '~'),
    ('1', '!'),
    ('2', '!!'),
    ('3', '!!!')
)

class TaskForm(forms.Form):

    taskTitle = forms.CharField(
        widget = forms.TextInput,
        label = 'Task Title',
#         initial = 'My task',
        help_text = 'Required. Pick something simple and informative.',
        validators = [
            validators.MinLengthValidator(2),
            validators.MaxLengthValidator(100)
        ],
        error_messages = {
            'required' : 'A task title is required. Please enter your title.',
            'min_length' : 'Title must be greater than 2 characters',
            'max_length' : 'Title must be less than 500 characters.'
        }
    )
    taskDescription = forms.CharField(
        label = 'Task Description',
        widget = forms.Textarea,
#         initial = 'What to do.',
        help_text = 'Describe the in a few words/sentences.',
        validators = [
            validators.MinLengthValidator(0),
            validators.MaxLengthValidator(500)
        ],
        error_messages = {
            'min_length' : 'Description must at least 1 character.',
            'max_length' : 'Description must be less than 500 characters.'
        }
    )
    taskPriority = forms.ChoiceField(
        choices=PRIORITY_CHOICES,
        label = 'Task Priority',
        widget = forms.Select,      
        initial = PRIORITY_CHOICES[0][0],
        help_text = 'Priority for this task.',

#       validators = [],
        error_messages = {
            'required' : 'A latitude coordinate is required. Please enter one.',
            'invalid_choice' : 'Invalid priority.'
        }
    )
    taskStatus = forms.BooleanField(
        required = False,
        label = 'Task Status',
#       widget = forms.TextInput,
        initial = False,
        help_text = 'Is it done yet?',
#       validators = [],
        error_messages = {
            'required' : 'Required?',
        }
    )
    now_hour_plus_one = (time.localtime().tm_hour - 4 + 1)
    now_pm = ('AM' if (now_hour_plus_one < 12) else 'PM')
    tdy = datetime.date.today()

    taskDuedate = forms.DateTimeField(
        label = 'Task Due Date',
        widget = forms.TextInput,
        initial = 'date',
        # initial = ('%04i/%02i/%02i %02i:00' % (tdy.year, tdy.month, tdy.day, now_hour_plus_one)), # today + top of next hour
        help_text = 'Required. At what time does the task need to be done?',
        input_formats=['%Y/%m/%d %H:%M'],
        # validators=[RegexValidator('^\d{4}/\d{2}/\d{2} \d{2}:\d{2} [-|+]\d{4}$', message='Invalid date/time format. Please enter your start date/time.')],
        error_messages = {
            'required' : 'A due date + time is required. Please enter your start date + time.',
            'invalid' : 'Invalid time format. Please enter your start date + time.'
        }
    )
    
    helper = FormHelper()
    helper.form_id = 'id-taskForm'
    helper.form_tag = True
    helper.form_class = 'form-horizontal'
    helper.field_template = 'hs_field_with_buttons.html'
    helper.label_class = 'col-md-2'
    helper.field_class = 'col-md-8'
    helper.layout = Layout(
        Div(
            Div('taskTitle',  css_class='row'),
            Div('taskDescription', css_class='row'),
            Div(
                Div('taskPriority', css_class='col-md-3'),
        #        Div('taskStatus', css_class='col-md-3'),
                Div('taskDuedate', css_class='col-md-6 datetimeparentdiv'),
                css_class='row'),
            css_class='col-md-12'),
        Button('Dismiss', 'dismiss', css_class='btn btn-warning btn-xs'),
        Submit('Update', 'update', css_class='ajax_submit btn btn-xs')
    )




class CommentForm(forms.Form):

    body = forms.CharField(
        label = 'Comment body',
        widget = forms.TextInput,
        initial = '',
        help_text = 'Please add your comment.',
        validators = [
            validators.MinLengthValidator(1),
            validators.MaxLengthValidator(500)
        ],
        error_messages = {
            'max_length' : 'Description must be at least 1 character.',
            'max_length' : 'Description must be less than 500 characters.'
        }
    )
    
    
    helper = FormHelper()
    # helper.form_id = 'id-commentForm'
    helper.form_tag = False
    helper.form_class = 'form-inline'
    helper.field_template = 'hs_inline_field.html'
#     helper.label_class = 'col-md-4'
#     helper.field_class = 'col-md-6'
    helper.layout = Layout(
        Div('body', css_class="col-md-8"),
        Div(
            Button('Dismiss', 'dismiss', css_class='btn btn-warning btn-xs'),
            Submit('Update', 'update', css_class='ajax_submit btn btn-xs'),
            css_class='col-md-4'
        )
    )
