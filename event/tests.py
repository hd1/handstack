#from django.test import TestCase
from unittest import TestCase
from django.contrib.auth.models import User

# Create your tests here.

class UserTestCase(TestCase):

    def setup(self):
        pass

    def test_user_create(self):
        """basic user creation test --- totally bogus example"""
        User.objects.create_user("bob", "bob@me.com", "1cheezy0")
        bob = User.objects.get(username="bob")
        print bob
        self.assertEqual(bob.email, "bob@me.com")
