# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Profile'
        db.create_table(u'event_profile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal(u'event', ['Profile'])

        # Adding model 'Usercategory'
        db.create_table(u'event_usercategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('group', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.Group'], unique=True)),
            ('description', self.gf('django.db.models.fields.CharField')(default=' ', max_length=50000)),
            ('lastedit', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='categoryLastedit', null=True, to=orm['auth.User'])),
        ))
        db.send_create_signal(u'event', ['Usercategory'])

        # Adding model 'Organization'
        db.create_table(u'event_organization', (
            (u'usercategory_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['event.Usercategory'], unique=True, primary_key=True)),
            ('orgAddress', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('orgLocationDescription', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('longitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('latitude', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal(u'event', ['Organization'])

        # Adding M2M table for field Admins on 'Organization'
        m2m_table_name = db.shorten_name(u'event_organization_Admins')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('organization', models.ForeignKey(orm[u'event.organization'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['organization_id', 'user_id'])

        # Adding model 'Interest'
        db.create_table(u'event_interest', (
            (u'usercategory_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['event.Usercategory'], unique=True, primary_key=True)),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('hashtag', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal(u'event', ['Interest'])

        # Adding model 'Location'
        db.create_table(u'event_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('locationName', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('locationAddress', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('locationDescription', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('lastedit', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='LocationLastedit', null=True, to=orm['auth.User'])),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('creationTime', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('longitude', self.gf('django.db.models.fields.FloatField')()),
            ('latitude', self.gf('django.db.models.fields.FloatField')()),
            ('rsvps', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal(u'event', ['Location'])

        # Adding model 'Frontline'
        db.create_table(u'event_frontline', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('fname', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('lname', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('organization', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('Comments', self.gf('django.db.models.fields.CharField')(max_length=50000)),
            ('creationTime', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'event', ['Frontline'])

        # Adding model 'Task'
        db.create_table(u'event_task', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('taskTitle', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('taskPriority', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('taskStatus', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('taskDuedate', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('taskCompletiondate', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('taskDescription', self.gf('django.db.models.fields.CharField')(max_length=5000)),
            ('lastedit', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='taskLastedit', null=True, to=orm['auth.User'])),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(related_name='taskCreator', to=orm['auth.User'])),
            ('creationTime', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('DueTime', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('CompletionTime', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'event', ['Task'])

        # Adding M2M table for field assignments on 'Task'
        m2m_table_name = db.shorten_name(u'event_task_assignments')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('task', models.ForeignKey(orm[u'event.task'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['task_id', 'user_id'])

        # Adding M2M table for field subtasks on 'Task'
        m2m_table_name = db.shorten_name(u'event_task_subtasks')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_task', models.ForeignKey(orm[u'event.task'], null=False)),
            ('to_task', models.ForeignKey(orm[u'event.task'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_task_id', 'to_task_id'])

        # Adding model 'Event'
        db.create_table(u'event_event', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('eventName', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('eventStartTime', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('eventAddress', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('image', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('eventDuration', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('eventDescription', self.gf('django.db.models.fields.CharField')(max_length=5000)),
            ('eventLocationDescription', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('longitude', self.gf('django.db.models.fields.FloatField')()),
            ('latitude', self.gf('django.db.models.fields.FloatField')()),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(related_name='eventCreator', to=orm['auth.User'])),
            ('lastedit', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='eventLastedit', null=True, to=orm['auth.User'])),
            ('creationTime', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('eventComputedStartTime', self.gf('django.db.models.fields.DateTimeField')()),
            ('eventComputedEndTime', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'event', ['Event'])

        # Adding M2M table for field Organizations on 'Event'
        m2m_table_name = db.shorten_name(u'event_event_Organizations')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'event.event'], null=False)),
            ('organization', models.ForeignKey(orm[u'event.organization'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'organization_id'])

        # Adding M2M table for field EventRSVPS on 'Event'
        m2m_table_name = db.shorten_name(u'event_event_EventRSVPS')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'event.event'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'user_id'])

        # Adding M2M table for field EventCheckins on 'Event'
        m2m_table_name = db.shorten_name(u'event_event_EventCheckins')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'event.event'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'user_id'])

        # Adding M2M table for field tasklist on 'Event'
        m2m_table_name = db.shorten_name(u'event_event_tasklist')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'event.event'], null=False)),
            ('task', models.ForeignKey(orm[u'event.task'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'task_id'])

        # Adding M2M table for field interests on 'Event'
        m2m_table_name = db.shorten_name(u'event_event_interests')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'event.event'], null=False)),
            ('interest', models.ForeignKey(orm[u'event.interest'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'interest_id'])

        # Adding model 'Comment'
        db.create_table(u'event_comment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('body', self.gf('django.db.models.fields.CharField')(max_length=50000)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('creationTime', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('lastedit', self.gf('django.db.models.fields.related.ForeignKey')(related_name='commentLastedit', to=orm['auth.User'])),
            ('parentEvent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['event.Event'])),
        ))
        db.send_create_signal(u'event', ['Comment'])

        # Adding model 'TaskComment'
        db.create_table(u'event_taskcomment', (
            (u'comment_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['event.Comment'], unique=True, primary_key=True)),
            ('parentTask', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['event.Task'])),
        ))
        db.send_create_signal(u'event', ['TaskComment'])


    def backwards(self, orm):
        # Deleting model 'Profile'
        db.delete_table(u'event_profile')

        # Deleting model 'Usercategory'
        db.delete_table(u'event_usercategory')

        # Deleting model 'Organization'
        db.delete_table(u'event_organization')

        # Removing M2M table for field Admins on 'Organization'
        db.delete_table(db.shorten_name(u'event_organization_Admins'))

        # Deleting model 'Interest'
        db.delete_table(u'event_interest')

        # Deleting model 'Location'
        db.delete_table(u'event_location')

        # Deleting model 'Frontline'
        db.delete_table(u'event_frontline')

        # Deleting model 'Task'
        db.delete_table(u'event_task')

        # Removing M2M table for field assignments on 'Task'
        db.delete_table(db.shorten_name(u'event_task_assignments'))

        # Removing M2M table for field subtasks on 'Task'
        db.delete_table(db.shorten_name(u'event_task_subtasks'))

        # Deleting model 'Event'
        db.delete_table(u'event_event')

        # Removing M2M table for field Organizations on 'Event'
        db.delete_table(db.shorten_name(u'event_event_Organizations'))

        # Removing M2M table for field EventRSVPS on 'Event'
        db.delete_table(db.shorten_name(u'event_event_EventRSVPS'))

        # Removing M2M table for field EventCheckins on 'Event'
        db.delete_table(db.shorten_name(u'event_event_EventCheckins'))

        # Removing M2M table for field tasklist on 'Event'
        db.delete_table(db.shorten_name(u'event_event_tasklist'))

        # Removing M2M table for field interests on 'Event'
        db.delete_table(db.shorten_name(u'event_event_interests'))

        # Deleting model 'Comment'
        db.delete_table(u'event_comment')

        # Deleting model 'TaskComment'
        db.delete_table(u'event_taskcomment')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'event.comment': {
            'Meta': {'object_name': 'Comment'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'body': ('django.db.models.fields.CharField', [], {'max_length': '50000'}),
            'creationTime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastedit': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'commentLastedit'", 'to': u"orm['auth.User']"}),
            'parentEvent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['event.Event']"})
        },
        u'event.event': {
            'EventCheckins': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'eventCheckins'", 'blank': 'True', 'to': u"orm['auth.User']"}),
            'EventRSVPS': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'eventrsvplist'", 'blank': 'True', 'to': u"orm['auth.User']"}),
            'Meta': {'object_name': 'Event'},
            'Organizations': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['event.Organization']", 'symmetrical': 'False'}),
            'creationTime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'eventCreator'", 'to': u"orm['auth.User']"}),
            'eventAddress': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'eventComputedEndTime': ('django.db.models.fields.DateTimeField', [], {}),
            'eventComputedStartTime': ('django.db.models.fields.DateTimeField', [], {}),
            'eventDescription': ('django.db.models.fields.CharField', [], {'max_length': '5000'}),
            'eventDuration': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'eventLocationDescription': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'eventName': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'eventStartTime': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'interests': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['event.Interest']", 'symmetrical': 'False'}),
            'lastedit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'eventLastedit'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'latitude': ('django.db.models.fields.FloatField', [], {}),
            'longitude': ('django.db.models.fields.FloatField', [], {}),
            'tasklist': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['event.Task']", 'symmetrical': 'False'})
        },
        u'event.frontline': {
            'Comments': ('django.db.models.fields.CharField', [], {'max_length': '50000'}),
            'Meta': {'object_name': 'Frontline'},
            'creationTime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'fname': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lname': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'organization': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'event.interest': {
            'Meta': {'object_name': 'Interest', '_ormbases': [u'event.Usercategory']},
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'hashtag': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'usercategory_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['event.Usercategory']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'event.location': {
            'Meta': {'object_name': 'Location'},
            'creationTime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastedit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'LocationLastedit'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'latitude': ('django.db.models.fields.FloatField', [], {}),
            'locationAddress': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'locationDescription': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'locationName': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'longitude': ('django.db.models.fields.FloatField', [], {}),
            'rsvps': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'event.organization': {
            'Admins': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'OrgAdmin'", 'blank': 'True', 'to': u"orm['auth.User']"}),
            'Meta': {'object_name': 'Organization', '_ormbases': [u'event.Usercategory']},
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'latitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'orgAddress': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'orgLocationDescription': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'usercategory_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['event.Usercategory']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'event.profile': {
            'Meta': {'object_name': 'Profile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'event.task': {
            'CompletionTime': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'DueTime': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'Task'},
            'assignments': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.User']", 'symmetrical': 'False'}),
            'creationTime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'taskCreator'", 'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastedit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'taskLastedit'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'subtasks': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['event.Task']", 'symmetrical': 'False'}),
            'taskCompletiondate': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'taskDescription': ('django.db.models.fields.CharField', [], {'max_length': '5000'}),
            'taskDuedate': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'taskPriority': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'taskStatus': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'taskTitle': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'event.taskcomment': {
            'Meta': {'object_name': 'TaskComment', '_ormbases': [u'event.Comment']},
            u'comment_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['event.Comment']", 'unique': 'True', 'primary_key': 'True'}),
            'parentTask': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['event.Task']"})
        },
        u'event.usercategory': {
            'Meta': {'object_name': 'Usercategory'},
            'description': ('django.db.models.fields.CharField', [], {'default': "' '", 'max_length': '50000'}),
            'group': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.Group']", 'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'lastedit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'categoryLastedit'", 'null': 'True', 'to': u"orm['auth.User']"})
        }
    }

    complete_apps = ['event']