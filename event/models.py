from django.db import models
from django.forms.models import model_to_dict
from django.contrib.auth.models import User, Group, Permission

from django.contrib import admin

from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

from reversion.helpers import patch_admin

from geopy.geocoders import Nominatim, GoogleV3


#from tzwhere import tzwhere;
import urllib2
import json
import datetime

# Create your models here.


class Profile(models.Model):
    image=models.FileField(upload_to='profiles')
    image_thumbnail =ImageSpecField(source='image',
                                      processors=[ResizeToFill(110, 110)],
                                      format='JPEG',
                                      options={'quality': 90})
    user = models.OneToOneField(User)
    class Meta:
        permissions = (
            ("general_notifications", "Recieve Notification Emails"),
        )

class Usercategory(models.Model):
    image=models.FileField(upload_to='groups')
    image_thumbnail =ImageSpecField(source='image',
                                      processors=[ResizeToFill(100, 100)],
                                      format='JPEG',
                                      options={'quality': 90})
    group=models.OneToOneField(Group)
    description = models.CharField(max_length=50000, default=" ")
    lastedit = models.ForeignKey(User,related_name="categoryLastedit",blank=True,null=True)
    def to_dict(self):
        grp= model_to_dict(self.group)
        cat= model_to_dict(self)
        cat['group']=grp
        cat['name']=self.group.name
        return cat
    def __str__(self):
        return self.group.name


class Organization(Usercategory):
    orgAddress=models.CharField(max_length=500, blank=True,null=True)
    orgLocationDescription = models.CharField(max_length=500, blank=True,null=True)
    longitude = models.FloatField( blank=True,null=True)
    latitude = models.FloatField(blank=True,null=True)
    Admins= models.ManyToManyField(User,related_name="OrgAdmin", blank=True)
    creator = models.ForeignKey(User)

class Interest(Usercategory):
    creator = models.ForeignKey(User)
    hashtag = models.CharField(max_length=500)



class Location(models.Model):
    locationName= models.CharField(max_length=500)
    locationAddress=models.CharField(max_length=500)
    locationDescription = models.CharField(max_length=500)
    #Hours?
    #
    lastedit = models.ForeignKey(User,related_name="LocationLastedit",blank=True,null=True)
    creator = models.ForeignKey(User)
    creationTime  = models.DateTimeField(auto_now_add=True)
    longitude = models.FloatField()
    latitude = models.FloatField()
    rsvps = models.CharField(max_length=500)
    def to_dict(self):
        return model_to_dict(self)





class Frontline(models.Model):
    email= models.CharField(max_length=500)
    fname = models.CharField(max_length=500)
    lname = models.CharField(max_length=500)
    organization = models.CharField(max_length=500)
    Comments = models.CharField(max_length=50000)
    creationTime  = models.DateTimeField(auto_now_add=True)
    def to_dict(self):
        return model_to_dict(self)
    def __str__(self):
        return self.email


class Task(models.Model):
    taskTitle= models.CharField(max_length=500)
    assignments = models.ManyToManyField(User)
    subtasks = models.ManyToManyField("self", symmetrical=False)
    taskPriority = models.CharField(max_length=500)
    taskStatus=models.CharField(max_length=500)
    taskDuedate = models.CharField(max_length=500)
    taskCompletiondate = models.CharField(max_length=500)
    taskDescription = models.CharField(max_length=5000)
    lastedit = models.ForeignKey(User,related_name="taskLastedit",blank=True,null=True)
    creator = models.ForeignKey(User,related_name="taskCreator")
    creationTime = models.DateTimeField(auto_now_add=True)
    taskComputedDuedate = models.DateTimeField(blank=True,null=True)
    taskComputedCompletiondate = models.DateTimeField(blank=True,null=True)
    def to_dict(self):
        ta=model_to_dict(self)
        parent=self.event_set.all()[0]
        tzd=self.taskComputedDuedate+ datetime.timedelta(seconds=parent.locationtzseconds)
#        tzc=self.taskComputedCompletiondate  + datetime.timedelta(seconds=parent.locationtzseconds)
	

        ta['dueday']=tzd.day
        ta['duedow']=sdays[tzd.weekday()]
        ta['duemonth']=smonths[tzd.month]
        ta['duehour']=tzd.hour
        ta['locationtzseconds']=parent.locationtzseconds
        ta['dueepoch'] = tzd.strftime("%s")
        ta['duetime']=tzd.strftime(" %I:%M %p").replace(" 0", "").lstrip();
        ta['dueminute']=tzd.minute
        # we'll add completion times later...
        return ta
    def __str__(self):
        return self.taskTitle

sdays=["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]
smonths=["nope","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]


class Event(models.Model):
    eventName= models.CharField(max_length=500)
    eventStartTime = models.CharField(max_length=500)
    eventAddress=models.CharField(max_length=500)
    image=models.FileField(upload_to='events')
    image_thumbnail =ImageSpecField(source='image',
                                      processors=[ResizeToFill(494, 180)],
                                      format='JPEG',
                                      options={'quality': 90})
    eventDuration = models.CharField(max_length=500)
    eventDescription = models.CharField(max_length=5000)
    eventLocationDescription = models.CharField(max_length=500)
    longitude = models.FloatField()
    latitude = models.FloatField()
    locationtzseconds=models.IntegerField(default=0)
    creator = models.ForeignKey(User,related_name="eventCreator")
    lastedit = models.ForeignKey(User,related_name="eventLastedit",blank=True,null=True)
    Organizations = models.ManyToManyField(Organization)
    creationTime = models.DateTimeField(auto_now_add=True)
    eventComputedStartTime = models.DateTimeField()
    eventComputedEndTime = models.DateTimeField()
    EventRSVPS = models.ManyToManyField(User,related_name="eventrsvplist", blank=True)
    EventCheckins = models.ManyToManyField(User,related_name="eventCheckins", blank=True)
    tasklist = models.ManyToManyField(Task)
    interests = models.ManyToManyField(Interest)

#    class Meta:
#        ordering = ('eventComputedStartTime')
    def to_dict(self):
        ev= model_to_dict(self)
#	w=tzwhere.tzwhere()

        tzs=self.eventComputedStartTime+ datetime.timedelta(seconds=self.locationtzseconds)
        tze=self.eventComputedEndTime  + datetime.timedelta(seconds=self.locationtzseconds)
        
#       ev["eventStartTime"]=self.eventComputedStartTime.strftime("%s")
#	ev['timezone']=w.tzNameAt(float(self.latitude),float(self.longitude))
        ev['startday']=tzs.day
        ev['startdow']=sdays[tzs.weekday()]
        ev['startmonth']=smonths[tzs.month]
        ev['starthour']=tzs.hour

        ev['starttime']=tzs.strftime(" %I:%M %p").replace(" 0", "").lstrip();
        ev['startminute']=tzs.minute
        
        ev['endday']=tze.day
        ev['enddow']=sdays[tze.weekday()]
        ev['endmonth']=smonths[tze.month]
        ev['endtime']=tze.strftime(" %I:%M %p").replace(" 0", "").lstrip();


        ev['durationhours']=float(self.eventDuration)/3600.0
        ev['endepoch']=tze.strftime("%s")
        ev['startepoch']=tzs.strftime("%s")
	
        return ev
    def __str__(self):
        return self.eventName
#     def save_geo(self, address, *args, **kwargs):
#         self.geocode(address)
#         super(Event, self).save(*args, **kwargs)
    def geocode(self, address):
	GoogleAPIkey='AIzaSyBvE5f36B-msKfzWdqshjqHIHgngXPBKc0'
        geolocator = GoogleV3(GoogleAPIkey) # Nominatim()
        location = geolocator.geocode(address)
        return (location.address, location.latitude, location.longitude)


class Comment(models.Model):
    body = models.CharField(max_length=50000)
    author=models.ForeignKey(User)
    creationTime  = models.DateTimeField(auto_now_add=True)
    lastedit = models.ForeignKey(User,related_name="commentLastedit")
    parentEvent=models.ForeignKey(Event, null=True, blank=True, default = None)
    parentTask=models.ForeignKey(Task, null=True, blank=True, default = None)
    def to_dict(self):
        cmnt=model_to_dict(self)
        cmnt["creationTime"]=self.creationTime.strftime("%Y-%m-%d %H:%M:%S %z")
#         cmnt["commentepoch"]=self.creationTime.strftime("%s")
        return cmnt
    def __str__(self):
        return self.author.username

# class TaskComment(Comment):
#     parentTask=models.ForeignKey(Task)




#class EventAdmin Admin(admin.ModelAdmin):
#    list_display = ('eventName', 'eventAddress', 'creator')

admin.site.register(Frontline)

admin.site.register(Event)
patch_admin(Event)

admin.site.register(Task)
patch_admin(Task)

admin.site.register(Organization)
patch_admin(Organization)

admin.site.register(Usercategory)
patch_admin(Usercategory)

admin.site.register(Profile)
patch_admin(Profile)

admin.site.register(Comment)
patch_admin(Comment)

admin.site.register(Interest)
patch_admin(Interest)
